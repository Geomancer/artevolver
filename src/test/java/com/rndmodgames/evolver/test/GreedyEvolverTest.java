package com.rndmodgames.evolver.test;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.rndmodgames.evolver.Utils;
import com.rndmodgames.evolver.impl.GreedyEvolver;

//import org.junit.Before;
//import org.junit.Test;

public class GreedyEvolverTest {

	// HARDCODED FOR Sherwin-Williams at 100% scale (all the colors)
	private static final int PALETTE_REPEATS     = 1;  // 1 palette : 24x44
	private static final int WIDTH_IN_TRIANGLES  = 24; // 2 palettes: 38x56
	private static final int HEIGHT_IN_TRIANGLES = 44; // 
	
	// evolver parameters
	private static final int POPULATION_SIZE = 1;
	private static final int MAX_POPULATION_SIZE = 1;
	
	private static final float TRIANGLE_WIDTH_PX = 5.1f;
	private static final float TRIANGLE_HEIGHT_PX = 3.8f;
	private static final float SCALE = 1.0f;
	
	@Before
	public void init() {
		// generate default data
//		GameDataLoader.initializeGameDataLoader(true);
	}

	@Test
	public void benchmarkGreedy() throws SQLException, IOException {
		
		// greedy evolver instance
		GreedyEvolver evolver = new GreedyEvolver(POPULATION_SIZE, MAX_POPULATION_SIZE);
		
		// load sample image from disk
		BufferedImage original = ImageIO.read(new File("images/cubos.jpg")); // cubos.jpg, plain.jpg, IMG_20161015_114000-2.jpg, pattern-tri.jpg, multicolor-hearts.png, pastel_box.jpg, a-colored-eye.png
		
		evolver.generate(original, SCALE,TRIANGLE_WIDTH_PX, TRIANGLE_HEIGHT_PX, WIDTH_IN_TRIANGLES, HEIGHT_IN_TRIANGLES, true, true);
		
		// number of triangles!
		int iterations = 2200;
		String background = "black"; // black, white, average, mirror
		
		// set background to render
		Utils.background = Color.WHITE;
//		Utils.background = Utils.averageColor(original, 0, 0, original.getWidth(), original.getHeight());
//		Utils.background = null;
//		Utils.backgroundImage = evolver.getResized();
		Utils.backgroundImage = null;
		Utils.mirror = false;
		
		// mirror only once!
//		RescaleOp op = new RescaleOp(-1.0f, 255f, null);
//		op.filter(Utils.backgroundImage, Utils.backgroundImage);
		
		String imageName = null;
		File outputfile = null;
		
		long then = System.currentTimeMillis();
		long now;
		long took;
		float secondsPerIteration;
		float estimatedLeft;
		
		for (int a = 1; a < iterations; a++){
			
			// iterate greedy algorithm
			evolver.iterate();
			
			// save best to disk
			imageName = "triangle_greedy_pastel_" + background + "_" + a;
			
			outputfile = new File("generated/" + imageName + ".png");
			ImageIO.write(evolver.getBestImage(), "png", outputfile);
			
			// print stats
			now = System.currentTimeMillis();
			took = now - then;
			
			secondsPerIteration = (float)(took / a) / 1000f;
			estimatedLeft = (float)(iterations - a) * secondsPerIteration;
			
			// 
			System.out.println(evolver.getColors().size() + ", " + a + ", " + evolver.getBestScore() + ", " + took + ", " + estimatedLeft);
		}
	}
}