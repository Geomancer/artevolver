package com.rndmodgames.evolver.test;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.rndmodgames.evolver.impl.TriangleEvolver;

//import org.junit.Before;
//import org.junit.Test;

public class TriangleEvolverTest {

	// HARDCODED FOR Sherwin-Williams at 100% scale (all the colors)
	private static final int WIDTH_IN_TRIANGLES  = 24; // 48
	private static final int HEIGHT_IN_TRIANGLES = 44; // 44
	
	// evolver parameters
	private static final int POPULATION_SIZE = 32;
	private static final int MAX_POPULATION_SIZE = 32;
	
	private static final float TRIANGLE_WIDTH_PX = 5.1f;
	private static final float TRIANGLE_HEIGHT_PX = 3.8f;
	private static final float SCALE = 1.0f;
	
	@Before
	public void init() {
		// generate default data
//		GameDataLoader.initializeGameDataLoader(true);
	}

	@Test
	public void benchmarkGreedy() throws SQLException, IOException {
		
	}
	
//	@Test
	public void benchmarkCrossoverPlusMutation() throws SQLException, IOException {

		// triangle evolver instance
		TriangleEvolver evolver = new TriangleEvolver(POPULATION_SIZE, MAX_POPULATION_SIZE);
		
		// load sample image from disk
		BufferedImage original = ImageIO.read(new File("images/a-colored-eye.png"));
		
		// init evolver with sample image
		evolver.init(original, SCALE, TRIANGLE_WIDTH_PX, TRIANGLE_HEIGHT_PX, WIDTH_IN_TRIANGLES, HEIGHT_IN_TRIANGLES);
		
		// evolve
		int iterations = 2400;
		int randomJumps = 10;
		int closeJumps = 10;
		int crossovers = 100;
		
		double currentBest = 0;

		for (int a = 0; a < iterations; a++){
			evolver.randomMutation(randomJumps);
			evolver.closeMutation(closeJumps);
			evolver.crossover(crossovers);
			
			if (evolver.getBestScore() > currentBest){
				
				currentBest = evolver.getBestScore();
				
				System.out.println("iteration: " + a + " best: " + currentBest);
				
				// save best to disk
				String imageName = "triangle_evolver_" + a;
				
				File outputfile = new File("generated/" + imageName + ".png");
				ImageIO.write(evolver.getBestImage(), "png", outputfile);
			}
		}
	}
	
//	@Test
	public void benchmarkCrossover() throws SQLException, IOException {

		// triangle evolver instance
		TriangleEvolver evolver = new TriangleEvolver(POPULATION_SIZE, MAX_POPULATION_SIZE);
		
		// load sample image from disk
		BufferedImage original = ImageIO.read(new File("images/a-colored-eye.png"));
		
		// init evolver with sample image
		evolver.init(original, SCALE, TRIANGLE_WIDTH_PX, TRIANGLE_HEIGHT_PX, WIDTH_IN_TRIANGLES, HEIGHT_IN_TRIANGLES);
		
		// evolve
		int crossovers = 10000;
		evolver.crossover(crossovers);
		
		// save best to disk
		String imageName = "triangle_evolver_" + crossovers + "_crossover_iterations_" + System.currentTimeMillis();
		
		File outputfile = new File("generated/" + imageName + ".png");
		ImageIO.write(evolver.getBestImage(), "png", outputfile);
	}
	
//	@Test
	public void benchmarkGenerateRandomImage() throws SQLException, IOException{
		
		// triangle evolver instance
		TriangleEvolver evolver = new TriangleEvolver(POPULATION_SIZE, MAX_POPULATION_SIZE);
		
		// load sample image from disk
		BufferedImage original = ImageIO.read(new File("images/a-colored-eye.png"));
				
		// init evolver with sample image
		evolver.init(original, SCALE, TRIANGLE_WIDTH_PX, TRIANGLE_HEIGHT_PX, WIDTH_IN_TRIANGLES, HEIGHT_IN_TRIANGLES);
		
		int iterations = 1000;
		boolean shortMutations = true;
		boolean randomMutations = true;
		int maxShortMutations = 3;
		
		// evolve
//		evolver.evolve(iterations, shortMutations, maxShortMutations, randomMutations);
		
		// save best to disk
		String imageName = "triangle_evolver_" + iterations + "_iterations_" + System.currentTimeMillis();
		
		File outputfile = new File("generated/" + imageName + ".png");
		ImageIO.write(evolver.getBestImage(), "png", outputfile);
	}
	
//	@Test
	public void testGenerateRandomImage() throws SQLException, IOException {
		
		// triangle evolver instance
		TriangleEvolver evolver = new TriangleEvolver(POPULATION_SIZE, MAX_POPULATION_SIZE);
		
		// load sample image from disk
		BufferedImage original = ImageIO.read(new File("images/a-colored-eye.png"));
		
		// init evolver with sample image
		evolver.init(original, SCALE, TRIANGLE_WIDTH_PX, TRIANGLE_HEIGHT_PX, WIDTH_IN_TRIANGLES, HEIGHT_IN_TRIANGLES);

		// save best to disk
		String imageName = "triangle_evolver_" + System.currentTimeMillis();
		
		File outputfile = new File("generated/" + imageName + ".png");
		ImageIO.write(evolver.getBestImage(), "png", outputfile);
	}
}