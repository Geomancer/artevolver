package com.rndmodgames.core.db;

import java.io.Serializable;
import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jboss.logging.Logger;

public abstract class GenericRepoImpl<E extends Serializable> {
	
	protected SessionFactory sessionFactory;
	private static Logger log = Logger.getLogger(GenericRepoImpl.class.getName());

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    public void save(E entity) throws SQLException {
    	try{
    		Session session = sessionFactory.getCurrentSession();
    		session.save(entity); 
    	} catch (org.hibernate.exception.ConstraintViolationException e){
    		// do nothing (duplicate key)
    		log.info("ignoring duplicate key!");
    	} catch(HibernateException e){
    		throw e;
    	}
    }
    
    public void update(E entity) throws SQLException {
    	try{
	    	Session session = sessionFactory.getCurrentSession();
			session.update(entity);
    	}catch(HibernateException e){
    		throw e;
    	}
    }
	
	public void delete(E entity) throws SQLException {
		try{
			Session session = sessionFactory.getCurrentSession();
			session.delete(entity);
		}catch(HibernateException e){
    		throw e;
    	}
	}
}