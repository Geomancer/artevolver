package com.rndmodgames.core.db;

import org.hibernate.dialect.MySQL57InnoDBDialect;

/**
 * Extends MySQL5InnoDBDialect and sets the default charset to be UTF-8
 * @author
 * Sorin Postelnicu
 * @since Aug 13, 2007
 */
public class MariaDBSQLDialect extends MySQL57InnoDBDialect {
	
	@Override
	public String getTableTypeString() {
		return " ENGINE=InnoDB DEFAULT CHARSET=utf8";
	}
}