package com.rndmodgames.core.db;

import org.hibernate.Session;
import org.jboss.logging.Logger;

public class HibernateUtils {

	static Logger log = Logger.getLogger(HibernateUtils.class.getName());
	
	public static void closeSession(Session session){
		try{
			if (session != null){
				session.close();
			}
		} catch (Exception e){
			log.debug("error closing the session (ignore)", e);
		}
	}
}