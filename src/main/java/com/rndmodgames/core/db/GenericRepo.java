package com.rndmodgames.core.db;

import java.io.Serializable;
import java.sql.SQLException;

import org.hibernate.SessionFactory;

public interface GenericRepo<E extends Serializable> {
    public void setSessionFactory(SessionFactory sessionFactory);
    public void save(E entity) throws SQLException;
    public void update(E entity) throws SQLException;
	public void delete(E entity) throws SQLException;
}