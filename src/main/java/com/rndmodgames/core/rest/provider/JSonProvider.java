//package com.rndmodgames.core.rest.provider;
//
//import javax.ws.rs.Consumes;
//import javax.ws.rs.Produces;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.ext.ContextResolver;
//import javax.ws.rs.ext.Provider;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
//
//@Provider
//@Produces(MediaType.APPLICATION_JSON)
//@Consumes(MediaType.APPLICATION_JSON)
//public class JSonProvider implements ContextResolver<ObjectMapper> {
//	
//	@Override
//    public ObjectMapper getContext(Class<?> type) {
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.registerModule(new Hibernate5Module());
//        return mapper;
//    }
//}