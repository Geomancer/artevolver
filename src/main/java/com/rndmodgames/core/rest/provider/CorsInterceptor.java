//package com.rndmodgames.core.rest.provider;
//
//import java.io.IOException;
//
//import javax.ws.rs.container.ContainerRequestContext;
//import javax.ws.rs.container.ContainerResponseContext;
//import javax.ws.rs.container.ContainerResponseFilter;
//
//import org.jboss.resteasy.spi.CorsHeaders;
//
//
//public class CorsInterceptor implements ContainerResponseFilter{
//	
//	//ContainerResponseFilters run after your JAX-RS resource method is invoked.
//	@Override
//	public void filter(ContainerRequestContext  requestContext,
//					   ContainerResponseContext responseContext) throws IOException {
//
//		responseContext.getHeaders().putSingle(CorsHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*");
//		responseContext.getHeaders().putSingle(CorsHeaders.ACCESS_CONTROL_ALLOW_HEADERS,"origin, content-type, accept, authorization, token, Link");
//		responseContext.getHeaders().putSingle(CorsHeaders.ACCESS_CONTROL_ALLOW_METHODS,"GET, POST, PUT, DELETE, OPTIONS, HEAD");
//		responseContext.getHeaders().putSingle(CorsHeaders.ACCESS_CONTROL_EXPOSE_HEADERS,"token, authorization, content-type, Link, X-Total-Count");
//		
//        //.header("Access-Control-Allow-Origin", "*")
//        //.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
//        //.header("Access-Control-Allow-Credentials", "true")
//		//.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
//		//.header("Access-Control-Max-Age", "1209600")
//
//	}
//
//}