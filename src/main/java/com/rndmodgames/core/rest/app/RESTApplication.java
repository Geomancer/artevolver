//package com.rndmodgames.core.rest.app;
//
//import java.util.HashSet;
//import java.util.Set;
//
//import javax.ws.rs.ApplicationPath;
//
//import com.payasosmediaticos.core.rest.TargetingService;
//import com.payasosmediaticos.core.rest.provider.CorsInterceptor;
//
//@ApplicationPath(value="")
//public class RESTApplication extends javax.ws.rs.core.Application {
//	
//	private Set<Object> singletons = new HashSet<Object>();
//
//	public RESTApplication () {
//	    singletons.add(new TargetingService());
//	    singletons.add(new CorsInterceptor());
//	}
//
//	@Override
//	public Set<Object> getSingletons() {
//	    return singletons;
//	}
//}