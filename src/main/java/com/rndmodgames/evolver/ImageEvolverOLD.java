package com.rndmodgames.evolver;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.rndmodgames.evolver.db.entities.Drawing;
import com.rndmodgames.evolver.db.entities.Experiment;
import com.rndmodgames.evolver.db.entities.PaletteColor;
import com.rndmodgames.evolver.db.entities.Population;
import com.rndmodgames.evolver.db.entities.Triangle;
import com.rndmodgames.evolver.db.repositories.ExperimentRepo;
import com.rndmodgames.evolver.viewmodel.EvolverVM;

public class ImageEvolverOLD{
	
	@Autowired ExperimentRepo experimentRepo;
	
	
	private String status;
	private Experiment experiment;
	
	private BufferedImage originalImage;
	private BufferedImage resizedOriginal;
	private BufferedImage framebuffer;
	
	// TODO: how do we match up to an experiment!
	private Drawing drawing;
	private BufferedImage bestImage;
	private Double bestScore;
	
	private static final boolean RANDOMIZE_PALETTE = true;
	private static final int RANDOM_PREGENERATIONS =  256; 
	
	static Logger log = Logger.getLogger(ImageEvolverOLD.class.getName());
	
	public ImageEvolverOLD(){
		experiment = new Experiment();
		experiment.setTotalDrawings(0L);
		experiment.setSize(0L);
		
		status = "Waiting";
	}

	// TODO: float vs double performance and accuracy
	// 
	public void initialize(float width, float height, int widthInTriangles, int heightInTriangles){
		
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		
		List <Population> allPop = new ArrayList<Population>();
		
		// create Population objects for the size of the experiment
		for (int a = 0; a < experiment.getMaxSize(); a++){
			Population population = new Population();
			population.setExperiment(experiment);
			population.setDrawings(new ArrayList<Drawing>());
			
			allPop.add(population);
		}

		experiment.setPopulations(allPop);
		experiment.setSize((long) allPop.size());
		
		// generate the framebuffer for triangle rendering (same size)
		framebuffer = Utils.getFramebuffer("Sherwin-Williams[100%]", 1.0f);
		
		// initialize and draw the resized original for comparison purposes
		resizedOriginal = Utils.getScaled(originalImage, framebuffer);

		for (int a = 0; a < experiment.getMaxSize(); a++) {
			
			// generate all the drawings
			for (int b = 0; b < experiment.getPopulationSize(); b++){
				
				System.out.println("drawing.n: " + b);
				
				// we only generate one drawing for each population for now
				drawing = generateRandomDrawing(width, height, widthInTriangles, heightInTriangles);
				drawing.setPopulation(experiment.getPopulations().get(a));
				drawing.setScore(0d);
					
				// add the drawing to the experiment
				experiment.getPopulations().get(a).getDrawings().add(drawing);
			}
		}
		
		// render initial drawing & update screen
		// TODO: all the drawings are generated equal now, but we should process and get the actual best one!
		Utils.renderDrawingToFramebuffer(experiment.getPopulations().get(0).getDrawings().get(0), framebuffer);
		bestImage = framebuffer;
	}
	
	public void update (){
		// no processing has been done
		if (experiment.getBestScore() == null){
			log.fine("There is not a best drawing, not processed.");
			return;
		}
		
		if (this.bestScore == null
				|| experiment.getBestScore() > this.bestScore){
			
			// search for the best drawing and render it to screen if all the data is there
			if (experiment.getBestPopulation() != null
					&& experiment.getBestPopulation().getBestDrawing() != null){
				
				// render the best drawing
				Utils.renderDrawingToFramebuffer(experiment.getBestPopulation().getBestDrawing(), framebuffer);
				
				// update screen
				bestImage = framebuffer;
				
			}else{
				log.fine("There is not a best drawing, probably needs more processing time.");
			}
		}

	}
	
	private Drawing generateRandomDrawing(float width, float height, int widthInTriangles, int heightInTriangles){
		
		Drawing drawing = new Drawing();

		// TODO: unhardcode/test
		float scale = 1.0f;
		
		ArrayList<Triangle> triangles = new ArrayList<Triangle>();
		ArrayList<Triangle> bestTriangles = new ArrayList<Triangle>();
		
		double bestScore = 0d;
		double score = 0d;
		
		for (int i = 0; i < RANDOM_PREGENERATIONS; i ++){
			// we only use each color once, so we keep track of that
			// we keep count to avoid repeating colors from a pallete
			// TODO: multiple triangles per color (1-4)
			triangles.clear();
			int count = 0;
			System.out.println("pregeneration.n: " + i);

			if (RANDOMIZE_PALETTE){
				Collections.shuffle(experiment.getPalette().getColors());
			}
			
			for (int a = 0; a < widthInTriangles; a++){
				for (int b = 0; b < heightInTriangles; b++){
					int xPoly [] = new int [3];
	    			int yPoly [] = new int [3];
	    			
	    			if (b % 2 == 0){
						xPoly[0] = (int) (width * scale * a);
						xPoly[1] = (int) ((width * scale * a) + (width * scale));
						xPoly[2] = (int) (width * scale * a);
						
						yPoly[0] = (int) ((height * scale * b));
						yPoly[1] = (int) ((height * scale * b));
						yPoly[2] = (int) ((height * scale * b) + (height * scale));
	    			}else{
	    				xPoly[0] = (int) ((width * scale * a));
						xPoly[1] = (int) ((width * scale * a) + (width * scale));
						xPoly[2] = (int) ((width * scale * a) + (width * scale));
						
						yPoly[0] = (int) ((height * scale * b));
						yPoly[1] = (int) ((height * scale * b));
						yPoly[2] = (int) ((height * scale * b) - (height * scale));
	    			}
	    			
	    			// dynamic row shifting
					yPoly[0] -= (height * scale) * (b / 2);
					yPoly[1] -= (height * scale) * (b / 2);
					yPoly[2] -= (height * scale) * (b / 2);

					PaletteColor color = null;
					
					if (experiment.getPalette().getColors().get(count) != null){
						color = experiment.getPalette().getColors().get(count);
						count++;
					}
					
					Triangle triangle = new Triangle(xPoly, yPoly, 3, color);
	        		triangles.add(triangle);
				}
			}
			
			// we set each pregeneration
			drawing.setTriangles(triangles);

			Utils.renderDrawingToFramebuffer(drawing, framebuffer);
			score = Utils.compare(resizedOriginal, framebuffer);
			
			if (score > bestScore){
				System.out.println("found best score: " + score);
				bestTriangles = triangles;
				bestScore = score;
			}
		}

		// this could fix the unsaved entity error, avoiding to keep in memory the pregenerated
		//	and discarted triangles without a parent reference
		for (Triangle triangle : bestTriangles){
			triangle.setDrawing(drawing);
		}
		
		// set the best generated
		drawing.setTriangles(bestTriangles);
		return drawing;
	}
	
	public void loadPaletteColorsFromTxt(String path) throws IOException{
		List <PaletteColor> colors = new ArrayList<PaletteColor>();
		
		URL url = getClass().getResource("../../../sherwin.txt");
		File file = new File(url.getPath());
		
		try (Stream<String>stream = Files.lines(file.toPath(), StandardCharsets.UTF_8)) {
			stream.forEach((line)->{
				PaletteColor color = null;
				String [] splitted = line.split(" ");

				if (splitted.length == 5){
					color = new PaletteColor(splitted[0],
											 splitted[1],
											 Integer.valueOf(splitted[2]).intValue(),
											 Integer.valueOf(splitted[3]).intValue(),
											 Integer.valueOf(splitted[4]).intValue());
				}else{
					if (splitted.length == 6){
						color = new PaletteColor(splitted[0],
									splitted[1] + " " + splitted[2],
									Integer.valueOf(splitted[3]).intValue(),
									Integer.valueOf(splitted[4]).intValue(),
									Integer.valueOf(splitted[5]).intValue());
					}else{
						if (splitted.length == 7){
							color = new PaletteColor(splitted[0],
													 splitted[1] + " " + splitted[2] + " " + splitted[3],
													 Integer.valueOf(splitted[4]).intValue(),
													 Integer.valueOf(splitted[5]).intValue(),
													 Integer.valueOf(splitted[6]).intValue());
						}else{
							System.out.println("error parsing pallete color!");
							System.out.println(line);
						}
					}
				}
				
				color.setPalette(experiment.getPalette());
				colors.add(color);
			});
		}
		
		// if the palette and or colors dont exist, we persist them to avoid loading this again in the future
		
		
		// set the palette to the experiment
		experiment.getPalette().setColors(colors);
	}
	
	public BufferedImage getOriginalImage() {
		return originalImage;
	}

	public void setOriginalImage(BufferedImage originalImage) {
		this.originalImage = originalImage;
	}

	public BufferedImage getBestImage() {
		return bestImage;
	}

	public void setBestImage(BufferedImage bestImage) {
		this.bestImage = bestImage;
	}

	public BufferedImage getFramebuffer() {
		return framebuffer;
	}

	public void setFramebuffer(BufferedImage framebuffer) {
		this.framebuffer = framebuffer;
	}

	public Experiment getExperiment() {
		return experiment;
	}

	public void setExperiment(Experiment experiment) {
		this.experiment = experiment;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getBestScore() {
		return bestScore;
	}

	public void setBestScore(Double bestScore) {
		this.bestScore = bestScore;
	}
}

//
//import java.awt.Color;
//import java.awt.Graphics;
//import java.awt.image.BufferedImage;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.List;
//
//import ec.util.MersenneTwisterFast;
//
//public class ImageEvolver {
//
//	public static final MersenneTwisterFast random = new MersenneTwisterFast();
//	public static final boolean KILL_PARENTS = false;
//	
//	private BufferedImage resizedOriginal;
//	private BufferedImage currentImage;
//	private BufferedImage bestImage;
//	private double bestScore;
//	
//	private int population;
//	private int randomJumpDistance;
//	private int crossoverMax;
//	private int totalIterations;
//	private int goodIterations;
//	
//	private float scale;
//	private float height;
//	private float width;
//	
//	private Pallete pallete;
//	
//	private static int triangleHeight;
//	private static int triangleWidth;
//	
//	CrossOver crossOver;
//	
//	private List<TriangleList<Triangle>> pop = new TriangleList<TriangleList<Triangle>>();
//	
//	private boolean isDirty = true;
//	private boolean initialized = false;
//	
//	@SuppressWarnings("static-access")
//	public ImageEvolver(int population, int randomJumpDistance, int crossoverMax, float scale, Pallete pallete, float width, float height, int triangleWidth, int triangleHeight){
//		this.population = population;
//		this.scale = scale;
//		this.pallete = pallete;
//		this.height = height;
//		this.width = width;
//		this.randomJumpDistance = randomJumpDistance;
//		this.crossoverMax = crossoverMax;
//		
//		this.triangleHeight = triangleHeight;
//		this.triangleWidth = triangleWidth;
//		
//		initCrossOver();
//	}
//	
//	public void initCrossOver(){
//		crossOver = new CrossOver(randomJumpDistance, crossoverMax);
//	}
//	
//	public void initialize(){
//		
//		int preGenerations = 1;
//		int randomMult = 1;
//		
//		for (int kk = 0; kk < preGenerations; kk++){
//			
//			pallete.randomize();
////			pallete.orderByLuminescence();
//			
//			for (int i = 0; i < population; i++){
//				TriangleList <Triangle> triangles = new TriangleList<Triangle>();
//				int count = 0;
//				
//				for (int a = 0; a < triangleWidth; a++){
//		    		for (int b = 0; b < triangleHeight; b++){
//		    			int xPoly [] = new int [3];
//		    			int yPoly [] = new int [3];
//	
//		    			if (b % 2 == 0){
//							xPoly[0] = (int) (width * scale * a);
//							xPoly[1] = (int) ((width * scale * a) + (width * scale));
//							xPoly[2] = (int) (width * scale * a);
//							
//							yPoly[0] = (int) ((height * scale * b));
//							yPoly[1] = (int) ((height * scale * b));
//							yPoly[2] = (int) ((height * scale * b) + (height * scale));
//		    			}else{
//		    				xPoly[0] = (int) ((width * scale * a));
//							xPoly[1] = (int) ((width * scale * a) + (width * scale));
//							xPoly[2] = (int) ((width * scale * a) + (width * scale));
//							
//							yPoly[0] = (int) ((height * scale * b));
//							yPoly[1] = (int) ((height * scale * b));
//							yPoly[2] = (int) ((height * scale * b) - (height * scale));
//		    			}
//		    			
//		    			// dynamic row shifting
//						yPoly[0] -= (height * scale) * (b / 2);
//						yPoly[1] -= (height * scale) * (b / 2);
//						yPoly[2] -= (height * scale) * (b / 2);
//	
//		    			Color color = null;
//		    			if (pallete.getColor(count) != null){
//		    				color = pallete.getColor(count).color;
//		    			}
//	
//		        		Triangle triangle = new Triangle(xPoly, yPoly, 3, color);
//		        		triangles.add(triangle);
//		        		
//		        		count++;
//		        	}
//		    	}
//
//				// randomize
//				for (int k = 0; k < triangles.size() * randomMult; k++){
//					switchColor(triangles, roll(triangles.size()), roll(triangles.size()));
//				}
//	
//				pop.add(triangles);
//			}
//			
//			int imgWidth = 385 - 140;
//        	int imgHeight = 167;
//			
//			BufferedImage imgParentA = null;
//        	Graphics g = null;
//        	double scoreA = 0d;
//        	
//        	for (TriangleList<Triangle> triangles : pop) {
//        		
//        		imgParentA = new BufferedImage(imgWidth, imgHeight, 1); 
//    			g = imgParentA.getGraphics();
//    			
//        		for (Triangle triangle : triangles){
//    				if (triangle.getColor() != null) {
//    					g.setColor(triangle.getColor());
//    					g.drawPolygon(triangle);
//    					g.fillPolygon(triangle);
//    				} else {
//    					g.setColor(Color.BLUE);
//    					g.drawPolygon(triangle);
//    				}
//        		}
//        		
//        		scoreA = compare(imgParentA, resizedOriginal);
//        		triangles.setScore(scoreA);
//        	}
//		}
//		
//		Collections.sort(pop, new TrianglesComparator());
//		
//		// keep only defined population
//    	pop = pop.subList(0, population);
//    	
//    	initialized = true;
//	}
//	
//	@SuppressWarnings("rawtypes")
//	public class TrianglesComparator implements Comparator<TriangleList> {
//	    @Override
//	    public int compare(TriangleList o1, TriangleList o2) {
//	    	return o1.getScore().compareTo(o2.getScore());
//	    }
//	}
//	
//	public static void switchColor(List<Triangle> triangles, int a, int b){
//		Triangle origin = triangles.get(a);
//		Triangle dest = triangles.get(b);
//		
//		Color aux = origin.getColor();
//		origin.setColor(dest.getColor());
//		dest.setColor(aux);
//	}
//	
//	public static void switchRandomColor(List<Triangle> triangles){
//		Triangle origin = triangles.get(roll(triangles.size()));
//		Triangle dest = triangles.get(roll(triangles.size()));
//		
//		Color aux = origin.getColor();
//		origin.setColor(dest.getColor());
//		dest.setColor(aux);
//	}
//	
//	// TODO implement randomization
//	public static void switchRandomMultiColor(List<Triangle> triangles, int maxTriangles){
//		
//		int origin = roll(triangles.size());
//		int dest = roll(triangles.size());
//		
//		if (origin >= triangles.size() - 1){
//			origin = triangles.size() - 2;
//		}
//		
//		if (dest >= triangles.size() - 1){
//			dest = triangles.size() - 2;
//		}
//		
//		Triangle triangle1 = triangles.get(origin);
//		Triangle triangle2 = triangles.get(origin + 1);
//		
//		Triangle triangle3 = triangles.get(dest);
//		Triangle triangle4 = triangles.get(dest + 1);
//
//		Color aux = triangle1.getColor();
//		triangle1.setColor(triangle3.getColor());
//		triangle3.setColor(aux);
//		
//		Color aux2 = triangle2.getColor();
//		triangle2.setColor(triangle4.getColor());
//		triangle4.setColor(aux2);
//	}
//	
//	public static void switchCloseColor(TriangleList<Triangle> triangles, int randomMutationsMax){
//		
//		int pos = roll(triangles.size());
//		int des = 0;
//		int jump = roll(randomMutationsMax);
//		
//		if(random.nextBoolean()){
//			des = pos + jump;
//		}else{
//			des = pos - jump;
//		}
//
//		if (des < 0){
//			des = (triangles.size() - 1) - pos - jump;
//		}
//
//		if (des >= triangles.size()){
//			des = 0 + (triangles.size() - pos + jump);
//		}
//		
//		Triangle origin = triangles.get(pos);
//		Triangle dest = triangles.get(des);
//		
//		Color aux = origin.getColor();
//		origin.setColor(dest.getColor());
//		dest.setColor(aux);
//	}
//	
//	public double compare(BufferedImage img1, BufferedImage img2) {
//		
//		int width1 = img1.getWidth(null);
//		int width2 = img2.getWidth(null);
//		int height1 = img1.getHeight(null);
//		int height2 = img2.getHeight(null);
//		
//		if ((width1 != width2) || (height1 != height2)) {
//			System.err.println("Error: Images dimensions mismatch");
//			return 0;
//		}
//		
//		boolean fitnessByColor = true;
//		long diff = 0;
//		
//		if (fitnessByColor){
//			for (int y = 0; y < height1; y++) {
//				for (int x = 0; x < width1; x++) {
//					int rgb1 = img1.getRGB(x, y);
//					int rgb2 = img2.getRGB(x, y);
//					int r1 = (rgb1 >> 16) & 0xff;
//					int g1 = (rgb1 >> 8) & 0xff;
//					int b1 = (rgb1) & 0xff;
//					int r2 = (rgb2 >> 16) & 0xff;
//					int g2 = (rgb2 >> 8) & 0xff;
//					int b2 = (rgb2) & 0xff;
//					diff += Math.abs(r1 - r2);
//					diff += Math.abs(g1 - g2);
//					diff += Math.abs(b1 - b2);
//				}
//			}
//		}else{
//			for (int y = 0; y < height1; y++) {
//				for (int x = 0; x < width1; x++) {
//					int rgb1 = img1.getRGB(x, y);
//					int rgb2 = img2.getRGB(x, y);
//					
//					float r1 = ((rgb1 >> 16) & 0xff) * 1f; 	// 0.299f
//					float g1 = ((rgb1 >> 8) & 0xff) * 0f;	// 0.587f
//					float b1 = ((rgb1) & 0xff) * 0f; 		// 0.114f
//					
//					float r2 = ((rgb2 >> 16) & 0xff) * 1f;
//					float g2 = ((rgb2 >> 8) & 0xff) * 0f;
//					float b2 = ((rgb2) & 0xff) * 0f;
//					
//					diff += Math.abs(r1 - r2);
//					diff += Math.abs(g1 - g2);
//					diff += Math.abs(b1 - b2);
//				}
//			}
//		}
//
//		double n = width1 * height1 * 3;
//		double p = diff / n / 255.0;
//		return 1 - p;
//	}
//	
//	public static int roll(int n){
//		return random.nextInt(n);
//	}
//
//	public int getPopulationSize() {
//		return population;
//	}
//
//	public void setPopulationSize(int population) {
//		this.population = population;
//	}
//
//	public List<TriangleList<Triangle>> getPopulation() {
//		return pop;
//	}
//
//	public void setPopulation(List<TriangleList<Triangle>> pop) {
//		this.pop = pop;
//	}
//
//	public BufferedImage getResizedOriginal() {
//		return resizedOriginal;
//	}
//
//	public void setResizedOriginal(BufferedImage resizedOriginal) {
//		this.resizedOriginal = resizedOriginal;
//	}
//
//	public BufferedImage getCurrentImage() {
//		return currentImage;
//	}
//
//	public void setCurrentImage(BufferedImage currentImage) {
//		this.currentImage = currentImage;
//	}
//
//	public BufferedImage getBestImage() {
//		return bestImage;
//	}
//
//	public void setBestImage(BufferedImage bestImage) {
//		this.bestImage = bestImage;
//	}
//
//	public int getTotalIterations() {
//		return totalIterations;
//	}
//
//	public void setTotalIterations(int totalIterations) {
//		this.totalIterations = totalIterations;
//	}
//
//	public int getGoodIterations() {
//		return goodIterations;
//	}
//
//	public void setGoodIterations(int goodIterations) {
//		this.goodIterations = goodIterations;
//	}
//
//	public double getBestScore() {
//		return bestScore;
//	}
//
//	public void setBestScore(double bestScore) {
//		this.bestScore = bestScore;
//	}
//
//	public boolean isDirty() {
//		return isDirty;
//	}
//
//	public void setDirty(boolean isDirty) {
//		this.isDirty = isDirty;
//	}
//
//	public boolean isInitialized() {
//		return initialized;
//	}
//
//	public void setInitialized(boolean initialized) {
//		this.initialized = initialized;
//	}
//
//	/**
//	 * Initial Random Population:
//	 * 	- order by score
//	 * 	- get top 10%
//	 * 	- mix and mutate
//	 * 	
//	 */
//	public void evolve() {
//		int iterations = 16;
//		
//		for (int a = 0; a < iterations; a++){
//			
//			int rollA = 0;
//			int rollB = 0;
//			
//			while (rollA == rollB){
//				rollA = roll(pop.size());
//				rollB = roll(pop.size());
//			}
//			
//			TriangleList <Triangle> parentA = pop.get(rollA);
//			TriangleList <Triangle> parentB = pop.get(rollB);
//			
//			TriangleList <Triangle> childA = crossOver.getChild(parentA, parentB);
//
//			int imgWidth = 385 - 140;
//        	int imgHeight = 167;
//        	
//        	BufferedImage imgParentA = null;
//        	Graphics g = null;
//        	double scoreA = 0d;
//
//        	if (parentA.getScore() <= 0d){
//        		imgParentA = new BufferedImage(imgWidth, imgHeight, 1); 
//    			g = imgParentA.getGraphics();
//    			
//    			for (Triangle triangle : parentA) {
//    				if (triangle.getColor() != null) {
//    					g.setColor(triangle.getColor());
//    					g.drawPolygon(triangle);
//    					g.fillPolygon(triangle);
//    				} else {
//    					g.setColor(Color.BLUE);
//    					g.drawPolygon(triangle);
//    				}
//    			}
//        		
//        		scoreA = compare(imgParentA, resizedOriginal);
//        		parentA.setScore(scoreA);
//        	}else{
//        		scoreA = parentA.getScore();
//        	}
//
//			BufferedImage imgChildA = new BufferedImage(imgWidth, imgHeight, 1); 
//			g = imgChildA.getGraphics();
//			
//			for (Triangle triangle : childA) {
//				if (triangle.getColor() != null) {
//					g.setColor(triangle.getColor());
//					g.drawPolygon(triangle);
//					g.fillPolygon(triangle);
//				} else {
//					g.setColor(Color.BLUE);
//					g.drawPolygon(triangle);
//				}
//			}
//
//			double scoreC = compare(imgChildA,  resizedOriginal);
//			childA.setScore(scoreC);
//			
//			if (scoreA > bestScore){
//				bestScore = scoreA;
//				bestImage = imgParentA;
//				goodIterations++;
//				
//				isDirty = true;
//			}
//			
//			// KILL PARENT (50/50) IF BEST
//			if (scoreC > scoreA){
//				pop.remove(parentA);
//				pop.add(childA);
//				goodIterations++;
//			}
//			
//			// BETTER IMAGE
//			if (scoreC > bestScore){
//				bestScore = scoreC;
//				bestImage = imgChildA;
//				goodIterations++;
//				
//				isDirty = true;
//			}
//			
//			totalIterations++;
//			
//			if (totalIterations % 1000 == 0){
//				System.out.println("i: " + totalIterations
//								 + " - p: " + pop.size()
//								 + " - jump: " + randomJumpDistance
//								 + " - cross: " + crossoverMax
//								 + " - best: " + bestScore);
//			}
//
//		}
//	}
//}