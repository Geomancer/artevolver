package com.rndmodgames.evolver.jobs;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.imageio.ImageIO;

import org.jboss.logging.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.rndmodgames.evolver.Utils;
import com.rndmodgames.evolver.db.entities.Drawing;
import com.rndmodgames.evolver.db.entities.Experiment;
import com.rndmodgames.evolver.db.entities.PaletteColor;
import com.rndmodgames.evolver.db.entities.Population;
import com.rndmodgames.evolver.db.entities.Triangle;
import com.rndmodgames.evolver.db.repositories.DrawingRepo;
import com.rndmodgames.evolver.db.repositories.ExperimentRepo;
import com.rndmodgames.evolver.db.repositories.PopulationRepo;
import com.rndmodgames.evolver.db.repositories.TriangleRepo;

public class ExperimentRunnerJob implements Job {

	@Autowired ExperimentRepo experimentRepo;
	@Autowired DrawingRepo drawingRepo;
	@Autowired TriangleRepo triangleRepo;
	@Autowired PopulationRepo populationRepo;
	
	static Logger log = Logger.getLogger(ExperimentRunnerJob.class.getName());
	
	static final int     MILLISECONDS_TO_RUN     = 600000; // 600k: 10 minutes
	static final int 	 ITERATIONS        	 	 =  10000; // 32-64-96-128

	static final float   RANDOM_JUMP_CHANCE      =  0.15f;
	static final int 	 MAX_RANDOM_JUMPS 	 	 =      2;
	
	static final float   CLOSE_JUMP_CHANCE       =  0.15f;
	static final int 	 MAX_CLOSE_JUMPS  	 	 =      2;
	static final int     MAX_CLOSE_JUMP_DISTANCE = 	    4;
	
	static final float   CROSSOVER_CHANCE        =  0.15f;
	static final int	 MAX_CROSSOVERS 	 	 =      2;
	
	static final int     VERTICAL_JUMP_STEPS     =     48;
	static final boolean IMAGE_CACHE_ENABLED 	 =   true;
	
	// caches
	private static ConcurrentHashMap<Long, Population> inProcess = new ConcurrentHashMap<Long, Population>();
	private static ConcurrentHashMap<Long, BufferedImage> imageCache = new ConcurrentHashMap<Long, BufferedImage>();
	private static ConcurrentHashMap<Long, Experiment> experimentCache = new ConcurrentHashMap<Long, Experiment>();

	@SuppressWarnings("unused")
	@Override
	public void execute(JobExecutionContext jExeCtx) throws JobExecutionException {
		long then = System.currentTimeMillis();
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);

		// framebuffer for triangle rendering
		BufferedImage framebuffer = Utils.getFramebuffer("Sherwin-Williams[100%]", 1.0f);
		BufferedImage targetImage;
		BufferedImage resizedOriginal;
		InputStream in;
		
		Population bestPopulation;
		Drawing populationBestDrawing;
		double populationBestScore;
		double fitness;
		
		boolean randomJump = false;
		boolean closeJump = false;
		boolean randomCrossover = false;
		
		List <Triangle> updatedTriangles = new ArrayList<Triangle>();
		List <Triangle> bestTriangles = new ArrayList<Triangle>();
		
		try {
			
			List<Experiment> processing = new ArrayList<Experiment>();
			Experiment exp;
			
			if (!experimentCache.contains(1L)){
				
				// 80-200 ms 
				exp = experimentRepo.get(1L);
				
				// only cache if is not running?
				if (exp != null && !exp.getRunning()){
					experimentCache.put(1L, exp);
				}
			}else{
				exp = experimentCache.get(1L);
			}
						
			processing.add(exp);

			// process n iterations for each experiment, persist best results
			for (Experiment experiment : processing){
				
				if (experiment == null || experiment.getRunning() == null || experiment.getRunning() || experiment.getFinished()){
					continue;
				}
				
				// persist the start of the experiment
				if (experiment.getStartRunning() == null){
					experiment.setStartRunning(new Date());
					experimentRepo.update(experiment);
					experimentCache.remove(1L);
				}
				
				// image caching to avoid regenerating
				if (IMAGE_CACHE_ENABLED && imageCache.contains(experiment.getId())){
					resizedOriginal = imageCache.get(experiment.getId());
				}else{
					in = new ByteArrayInputStream(experiment.getTargetImage());
					targetImage = ImageIO.read(in);
					resizedOriginal = Utils.getScaled(targetImage, framebuffer);
					
					imageCache.put(experiment.getId(), resizedOriginal);
				}
				
				for (int a = 0; a < ITERATIONS; a++){
					for (Population population : experiment.getPopulations()){
						
						// avoid processing the same
						if (inProcess.contains(population.getId())){
							continue;
						}else{
							inProcess.put(population.getId(), population);
						}
						
						for (Drawing drawing : population.getDrawings()){

							// reset the updated triangles to avoid persisting again
							updatedTriangles.clear();
							
							if (Utils.random.nextFloat() <= RANDOM_JUMP_CHANCE){
								// random triangle jumps
								updatedTriangles.addAll(randomTriangleJump(drawing, Utils.random(MAX_RANDOM_JUMPS)));
							}
							
							if (Utils.random.nextFloat() <= CLOSE_JUMP_CHANCE){
								// close triangle jump
								updatedTriangles.addAll(closeTriangleJump(drawing, Utils.random(MAX_CLOSE_JUMPS)));
							}
							
							if (Utils.random.nextFloat() <= CROSSOVER_CHANCE){
								// first develop, then move to a method
								for (int b = 0; b < Utils.random(MAX_CROSSOVERS); b++){
									// parentAB for easy crossover
									Drawing parentA = drawing;
									Drawing parentB = null;
									
									do {
										parentB = population.getDrawings().get(Utils.random(population.getDrawings().size() - 1));
									} while (parentA.equals(parentB));
									
									// we have different parents, so we move triangles from parentB to this one and check if its better
									List <Triangle> randomTriangles = new ArrayList<Triangle>();
									
									for (int i = 0; i < MAX_CROSSOVERS; i++){
										int randomPosition = Utils.random.nextInt(parentB.getTriangles().size() - 1);
										Triangle triangle = parentB.getTriangles().get(randomPosition);
										randomTriangles.add(triangle);
									}
									
									// swap the triangle colors
									for (int i = 0; i < randomTriangles.size(); i ++){
										// iterate current drawing
										Triangle triangleB = randomTriangles.get(i);
										Triangle triangleA = null;
										
										int destinationIndex = -1;
										int originIndex = -1;
										
										for (int j = 0; j < parentA.getTriangles().size(); j ++){
											
											triangleA = parentA.getTriangles().get(j);

											if (Arrays.equals(triangleA.getxPoly(), triangleB.getxPoly())
													&& Arrays.equals(triangleA.getyPoly(), triangleB.getyPoly())) {
												
												destinationIndex = j;
											}
											
											if (triangleA.getColor().equals(triangleB.getColor())){
												originIndex = j;
											}
										}

										if (destinationIndex >= 0 && originIndex >= 0 && (destinationIndex != originIndex)){
											
											Triangle swap = parentA.getTriangles().get(originIndex);
											PaletteColor auxColor = swap.getColor();
											
											swap.setColor(triangleA.getColor());
											triangleA.setColor(auxColor);
											
											updatedTriangles.add(swap);
											updatedTriangles.add(triangleA);										
										}
									}
								}
							}

							// draw to the correct size buffer
							Utils.renderDrawingToFramebuffer(drawing, framebuffer);
							fitness = Utils.compare(resizedOriginal, framebuffer);
							
							// update the experiment before checking the iteration count
							if (experiment.getIterations() == null){
								experiment.setIterations(1L);
							}else{
								experiment.setIterations(experiment.getIterations() + 1L);
							}
							
							// if it is better overall keep as better drawing/score
							if (drawing.getScore() == null
									|| (drawing.getScore() >= 0 
											&& (fitness > drawing.getScore()))){
								
								// update the best fitness
								drawing.setScore(fitness);
								
								// persist drawing
								drawingRepo.update(drawing);
								experimentCache.remove(1L);
								
								// update best triangles
								for (Triangle triangle : updatedTriangles){
									triangleRepo.update(triangle);
								}
								
								// update the count of successful runs
								if (experiment.getSuccessIterations() == null){
									experiment.setSuccessIterations(1L);
								}else{
									experiment.setSuccessIterations(experiment.getSuccessIterations() + 1L);
								}
							}
						}
						
						for (Drawing drawing : population.getDrawings()){
							if (population.getBestScore() == null
									|| drawing.getScore() > population.getBestScore()){
								
								population.setBestScore(drawing.getScore());
								population.setBestDrawing(drawing);
								
								populationRepo.update(population);
								experimentCache.remove(1L);
							}
						}
					}
					
					for (Population population : experiment.getPopulations()){
						// if experiment got better
						if (experiment.getBestScore() == null
								|| (population.getBestScore() > experiment.getBestScore()) ){
							
							experiment.setBestScore(population.getBestScore());
							experiment.setBestPopulation(population);

							// hardcoded evict cache
							experimentCache.remove(1L);
						}
					}
				}
				
				// persist each experiment once after iterations
				long milliseconds = new Date().getTime() - experiment.getStartRunning().getTime();
				
				// stop processing if we finished
				if (milliseconds >= MILLISECONDS_TO_RUN){
					experiment.setFinished(true);
				}
				
				experiment.setSecondsRunning(milliseconds/1000);
				experimentRepo.update(experiment);
			}
		} catch (Exception e){
			log.error("Error executing scraping job!", e);
		}

		long now = System.currentTimeMillis();
		log.info("ExperimentRunnerJob Finished. Took " + (now-then) + " ms");
	}
	
	private List<Triangle> randomTriangleJump(Drawing drawing, int jumps){
		
		// keep track of updated triangles just in case we get a better
		//	fitness and need to persist only those
		
		List<Triangle> updatedTriangles = new ArrayList<Triangle>();

		for (int a = 0; a < jumps; a++){

			// simple random triangle jump
			int positionA = 0;
			int positionB = 0;
			
			// do not swap itself
			while(positionA == positionB){
				positionA = Utils.random.nextInt(drawing.getTriangles().size());
				positionB = Utils.random.nextInt(drawing.getTriangles().size());
			}
			
			// we need an auxiliar triangle to avoid overwritting
			Triangle triangleA = drawing.getTriangles().get(positionA);
			Triangle triangleB = drawing.getTriangles().get(positionB);

			// we swap the triangle position and color (we need a temp variable)
			PaletteColor auxColor = triangleA.getColor();
			
			// swap colors
			triangleA.setColor(triangleB.getColor());
			triangleB.setColor(auxColor);
			
			updatedTriangles.add(triangleA);
			updatedTriangles.add(triangleB);
		}
		
		return updatedTriangles;
	}
	
	private List<Triangle> closeTriangleJump(Drawing drawing, int jumps){
		List<Triangle> updatedTriangles = new ArrayList<Triangle>();

		for (int a = 0; a < jumps; a++){

			// simple random triangle jump
			int positionA = Utils.random.nextInt(drawing.getTriangles().size());
			int positionB = 0;
			int jumpDistance = Utils.random(MAX_CLOSE_JUMP_DISTANCE);

			boolean horizontal = Utils.random.nextBoolean();
			boolean vertical = Utils.random.nextBoolean();
			
			if (horizontal){
				positionB = positionA + (jumpDistance * (vertical ? 1 : -1));

				// test
				if (positionB >= drawing.getTriangles().size()){
					positionB = (positionB - drawing.getTriangles().size());
				}
			}else{
				
				// swap left
				positionB = positionA + (horizontal ? VERTICAL_JUMP_STEPS : VERTICAL_JUMP_STEPS * -1);

				// test
				if (positionB < 0){
					positionB = (drawing.getTriangles().size() + positionB);
				}
			}

			// we need an auxiliar triangle to avoid overwritting
			Triangle triangleA = drawing.getTriangles().get(positionA);
			Triangle triangleB = drawing.getTriangles().get(positionB);

			// we swap the triangle position and color (we need a temp variable)
			PaletteColor auxColor = triangleA.getColor();
			
			// swap colors
			triangleA.setColor(triangleB.getColor());
			triangleB.setColor(auxColor);
			
			updatedTriangles.add(triangleA);
			updatedTriangles.add(triangleB);
		}
		
		return updatedTriangles;
	}
}