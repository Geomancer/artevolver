package com.rndmodgames.evolver.db.repositories;

import com.rndmodgames.core.db.GenericRepo;
import com.rndmodgames.evolver.db.entities.Triangle;

public interface TriangleRepo extends GenericRepo<Triangle>{
	public abstract Triangle get(Long id) throws Exception;
}