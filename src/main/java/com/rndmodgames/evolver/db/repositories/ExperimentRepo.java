package com.rndmodgames.evolver.db.repositories;

import java.util.List;

import com.rndmodgames.core.db.GenericRepo;
import com.rndmodgames.evolver.db.entities.Experiment;

public interface ExperimentRepo extends GenericRepo<Experiment>{
	public abstract List <Experiment> getAll() throws Exception;
	public abstract Experiment get(Long id) throws Exception;
	public abstract void persist(Experiment experiment) throws Exception;
}