package com.rndmodgames.evolver.db.repositories;

import java.util.List;

import com.rndmodgames.core.db.GenericRepo;
import com.rndmodgames.evolver.db.entities.Palette;

public interface PaletteRepo extends GenericRepo<Palette>{
	public abstract List <Palette> getAll() throws Exception;
}