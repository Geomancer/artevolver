package com.rndmodgames.evolver.db.repositories.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.jboss.logging.Logger;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.rndmodgames.core.db.GenericRepoImpl;
import com.rndmodgames.core.db.HibernateUtils;
import com.rndmodgames.evolver.db.entities.Experiment;
import com.rndmodgames.evolver.db.repositories.ExperimentRepo;

public class ExperimentRepoImpl extends GenericRepoImpl<Experiment> implements ExperimentRepo {

	private static Logger log = Logger.getLogger(ExperimentRepoImpl.class.getName());
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Experiment> getAll() throws Exception {
		Session session = sessionFactory.openSession();
		
		try {
			String query = "from Experiment experiment"
						 + " left join fetch experiment.populations populations"
						 + " group by experiment.id";

			return (List<Experiment>) session.createQuery(query).list();

		} catch (Exception e){
			throw e;
		} finally {
			HibernateUtils.closeSession(session);
		}
	}
	
	@Override
	public Experiment get(Long id) throws Exception {
		Session session = sessionFactory.openSession();
		
		try {
			String query = "from Experiment experiment "
						 + "	left join fetch experiment.populations populations"
						 + "	where experiment.id = :id"
						 + " 	group by experiment.id";

			return (Experiment) session.createQuery(query)
									.setLong("id", id)
									.uniqueResult();

		} catch (Exception e){
			throw e;
		} finally {
			HibernateUtils.closeSession(session);
		}
	}

	@Override
	public void persist(Experiment experiment) throws Exception {
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		// iterate the experiment populations and persist them
		for (int a = 0; a < experiment.getPopulations().size(); a++){
			try{
				// persist population
				session.save(experiment.getPopulations().get(a));
				
				// iterate the drawings
				for (int b = 0; b < experiment.getPopulations().get(a).getDrawings().size(); b++){
					
					// persist the drawing
					session.save(experiment.getPopulations().get(a).getDrawings().get(b));
				}
			}catch (Exception e){
				log.info("tried to persist experiment error", e);
			}
		}
		
		// iterate the triangles
		for (int a = 0; a < experiment.getPopulations().size(); a++){
			for (int b = 0; b < experiment.getPopulations().get(a).getDrawings().size(); b++){
				for (int c = 0; c < experiment.getPopulations().get(a).getDrawings().get(b).getTriangles().size(); c++){
					// persist the triangle
					session.save(experiment.getPopulations().get(a).getDrawings().get(b).getTriangles().get(c));
					
					// 20, same as the JDBC batch size [hibernate.jdbc.batch_size]
					if ( c % 20 == 0 ) {
				        session.flush();
				        session.clear();
					}
				}
			}
		}

		// persist other stuff
		experiment.setRunning(false);
		session.update(experiment);
		
		tx.commit();
		HibernateUtils.closeSession(session);
	}
}