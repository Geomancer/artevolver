package com.rndmodgames.evolver.db.repositories;

import com.rndmodgames.core.db.GenericRepo;
import com.rndmodgames.evolver.db.entities.Drawing;

public interface DrawingRepo extends GenericRepo<Drawing>{
	public abstract Drawing get(Long id) throws Exception;
}