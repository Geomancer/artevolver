package com.rndmodgames.evolver.db.repositories.impl;

import org.hibernate.Session;

import com.rndmodgames.core.db.GenericRepoImpl;
import com.rndmodgames.core.db.HibernateUtils;
import com.rndmodgames.evolver.db.entities.Drawing;
import com.rndmodgames.evolver.db.repositories.DrawingRepo;

public class DrawingRepoImpl extends GenericRepoImpl<Drawing> implements DrawingRepo {
	
	@Override
	public Drawing get(Long id) throws Exception {
		Session session = sessionFactory.openSession();
		
		try {
			String query = "from Drawing drawing where drawing.id = :id";

			return (Drawing) session.createQuery(query)
									.setLong("id", id)
									.uniqueResult();

		} catch (Exception e){
			throw e;
		} finally {
			HibernateUtils.closeSession(session);
		}
	}
}