package com.rndmodgames.evolver.db.repositories.impl;

import org.hibernate.Session;

import com.rndmodgames.core.db.GenericRepoImpl;
import com.rndmodgames.core.db.HibernateUtils;
import com.rndmodgames.evolver.db.entities.Triangle;
import com.rndmodgames.evolver.db.repositories.TriangleRepo;

public class TriangleRepoImpl extends GenericRepoImpl<Triangle> implements TriangleRepo {
	
	@Override
	public Triangle get(Long id) throws Exception {
		Session session = sessionFactory.openSession();
		
		try {
			String query = "from Triangle triangle where triangle.id = :id";

			return (Triangle) session.createQuery(query)
									.setLong("id", id)
									.uniqueResult();

		} catch (Exception e){
			throw e;
		} finally {
			HibernateUtils.closeSession(session);
		}
	}
}