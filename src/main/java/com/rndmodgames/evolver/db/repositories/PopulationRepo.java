package com.rndmodgames.evolver.db.repositories;

import com.rndmodgames.core.db.GenericRepo;
import com.rndmodgames.evolver.db.entities.Population;

public interface PopulationRepo extends GenericRepo<Population>{
	
}