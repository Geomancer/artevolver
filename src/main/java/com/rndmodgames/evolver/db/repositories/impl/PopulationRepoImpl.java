package com.rndmodgames.evolver.db.repositories.impl;

import com.rndmodgames.core.db.GenericRepoImpl;
import com.rndmodgames.evolver.db.entities.Population;
import com.rndmodgames.evolver.db.repositories.PopulationRepo;

public class PopulationRepoImpl extends GenericRepoImpl<Population> implements PopulationRepo {

}