package com.rndmodgames.evolver.db.repositories.impl;

import java.util.List;

import org.hibernate.Session;

import com.rndmodgames.core.db.GenericRepoImpl;
import com.rndmodgames.core.db.HibernateUtils;
import com.rndmodgames.evolver.db.entities.PaletteColor;
import com.rndmodgames.evolver.db.repositories.PaletteColorRepo;

public class PaletteColorRepoImpl extends GenericRepoImpl<PaletteColor> implements PaletteColorRepo {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PaletteColor> getAll(Long id) throws Exception {
		Session session = sessionFactory.openSession();
		
		try {
			String query = "from PaletteColor color where color.palette.id = :id";

			return (List<PaletteColor>) session.createQuery(query)
											   .setLong("id", id)
											   .list();

		} catch (Exception e){
			throw e;
		} finally {
			HibernateUtils.closeSession(session);
		}
	}
}