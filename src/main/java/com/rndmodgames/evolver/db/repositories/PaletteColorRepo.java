package com.rndmodgames.evolver.db.repositories;

import java.util.List;

import com.rndmodgames.core.db.GenericRepo;
import com.rndmodgames.evolver.db.entities.PaletteColor;

public interface PaletteColorRepo extends GenericRepo<PaletteColor>{
	public abstract List<PaletteColor> getAll(Long id) throws Exception;
}