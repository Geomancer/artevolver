package com.rndmodgames.evolver.db.repositories.impl;

import java.util.List;

import org.hibernate.Session;

import com.rndmodgames.core.db.GenericRepoImpl;
import com.rndmodgames.core.db.HibernateUtils;
import com.rndmodgames.evolver.db.entities.Palette;
import com.rndmodgames.evolver.db.repositories.PaletteRepo;

public class PaletteRepoImpl extends GenericRepoImpl<Palette> implements PaletteRepo {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Palette> getAll() throws Exception {
		Session session = sessionFactory.openSession();
		
		try {
			String query = "from Palette palette";

			return (List<Palette>) session.createQuery(query).list();

		} catch (Exception e){
			throw e;
		} finally {
			HibernateUtils.closeSession(session);
		}
	}
}