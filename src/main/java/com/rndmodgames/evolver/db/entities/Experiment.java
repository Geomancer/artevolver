package com.rndmodgames.evolver.db.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "experiment")
public class Experiment implements Serializable{

	private static final long serialVersionUID = -629293076417846934L;
	
	private Long id;
	private Boolean running;
	private Boolean finished;
	private Date startRunning;
	private Long secondsRunning;
	private byte [] targetImage;
	private Palette palette;
	private Long maxSize;
	private Long size;
	private Long populationSize;
	private List <Population> populations;
	private Long totalDrawings;
	private Long iterations;
	private Long successIterations;
	
	// record keeping
	private Double bestScore;
	private Population bestPopulation;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getRunning() {
		return running;
	}

	public void setRunning(Boolean running) {
		this.running = running;
	}

	public Boolean getFinished() {
		return finished;
	}

	public void setFinished(Boolean finished) {
		this.finished = finished;
	}

	@Column(name="start_running")
	public Date getStartRunning() {
		return startRunning;
	}

	public void setStartRunning(Date startRunning) {
		this.startRunning = startRunning;
	}

	@Column(name="seconds_running")
	public Long getSecondsRunning() {
		return secondsRunning;
	}

	public void setSecondsRunning(Long secondsRunning) {
		this.secondsRunning = secondsRunning;
	}

	@Column(name="target_image", length=1500000)
	public byte [] getTargetImage() {
		return targetImage;
	}

	public void setTargetImage(byte [] targetImage) {
		this.targetImage = targetImage;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "palette_id", nullable = false)
	public Palette getPalette() {
		return palette;
	}

	public void setPalette(Palette palette) {
		this.palette = palette;
	}

	@Column(name="max_size")
	public Long getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(Long maxSize) {
		this.maxSize = maxSize;
	}

	@Column
	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	@OneToMany(mappedBy="experiment", fetch = FetchType.EAGER)
	public List<Population> getPopulations() {
		return populations;
	}

	@Column(name="population_size")
	public Long getPopulationSize() {
		return populationSize;
	}

	public void setPopulationSize(Long populationSize) {
		this.populationSize = populationSize;
	}

	public void setPopulations(List<Population> populations) {
		this.populations = populations;
	}

	@Transient
	public Long getTotalDrawings() {
		return totalDrawings;
	}

	public void setTotalDrawings(Long totalDrawings) {
		this.totalDrawings = totalDrawings;
	}

	public Long getIterations() {
		return iterations;
	}

	public void setIterations(Long iterations) {
		this.iterations = iterations;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "best_population_id", nullable = true)
	public Population getBestPopulation() {
		return bestPopulation;
	}

	public void setBestPopulation(Population bestPopulation) {
		this.bestPopulation = bestPopulation;
	}

	public Double getBestScore() {
		return bestScore;
	}

	public void setBestScore(Double bestScore) {
		this.bestScore = bestScore;
	}

	@Column(name="success_iterations")
	public Long getSuccessIterations() {
		return successIterations;
	}

	public void setSuccessIterations(Long successIterations) {
		this.successIterations = successIterations;
	}
}