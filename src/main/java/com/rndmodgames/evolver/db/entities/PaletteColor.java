package com.rndmodgames.evolver.db.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "palette_color")
public class PaletteColor implements Serializable {

	private static final long serialVersionUID = -1430981047384031865L;
	
	private Long id;
	
	private Palette palette;
	private String name;
	private String description;
	
	private int r;
	private int g;
	private int b;
	
	// default constructor for Hibernate
	public PaletteColor() {}
	
	public PaletteColor(String name, String description, int r, int g, int b){
		this.name = name;
		this.description = description;
		this.r = r;
		this.g = g;
		this.b = b;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "palette_id", nullable = false)
	public Palette getPalette() {
		return palette;
	}

	public void setPalette(Palette palette) {
		this.palette = palette;
	}

	@Column(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name="r")
	public int getR() {
		return r;
	}

	public void setR(int r) {
		this.r = r;
	}

	@Column(name="g")
	public int getG() {
		return g;
	}

	public void setG(int g) {
		this.g = g;
	}

	@Column(name="b")
	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaletteColor other = (PaletteColor) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}