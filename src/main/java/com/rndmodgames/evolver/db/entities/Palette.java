package com.rndmodgames.evolver.db.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "palette")
public class Palette implements Serializable{

	private static final long serialVersionUID = -8479714970494896042L;
	
	private Long id;
	private String name;
	private String description;
	private BigDecimal width;
	private BigDecimal height;
	
	private List <PaletteColor> colors = new ArrayList<PaletteColor>();

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(mappedBy="palette", fetch = FetchType.EAGER)
	public List<PaletteColor> getColors() {
		return colors;
	}

	public void setColors(List<PaletteColor> colors) {
		this.colors = colors;
	}

	@Column(precision=10, scale=2)
	public BigDecimal getWidth() {
		return width;
	}

	public void setWidth(BigDecimal width) {
		this.width = width;
	}

	@Column(precision=10, scale=2)
	public BigDecimal getHeight() {
		return height;
	}

	public void setHeight(BigDecimal height) {
		this.height = height;
	}
}


//package com.rndmodgames.evolver;
//
//import java.io.File;
//import java.io.IOException;
//import java.net.URL;
//import java.nio.charset.StandardCharsets;
//import java.nio.file.Files;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.List;
//import java.util.stream.Stream;
//
//public class Pallete {
//
//	String name;
//	List <PalleteColor> colors = new ArrayList<PalleteColor>();
//	
//	public Pallete(String name, int repetitions) throws IOException{
//		this.name = name;
//
//		URL url = getClass().getResource("../../../sherwin.txt");
//		File file = new File(url.getPath());
//		
//		try (Stream<String>stream = Files.lines(file.toPath(), StandardCharsets.UTF_8)) {
//			stream.forEach((line)->{
//				PalleteColor color = null;
//				String [] splitted = line.split(" ");
//
//				if (splitted.length == 5){
//					color = new PalleteColor(this,
//											 Long.valueOf(splitted[0]),
//											 splitted[1],
//											 Integer.valueOf(splitted[2]).intValue(),
//											 Integer.valueOf(splitted[3]).intValue(),
//											 Integer.valueOf(splitted[4]).intValue());
//				}else{
//					if (splitted.length == 6){
//						color = new PalleteColor(this,
//								 Long.valueOf(splitted[0]),
//								 splitted[1] + " " + splitted[2],
//								 Integer.valueOf(splitted[3]).intValue(),
//								 Integer.valueOf(splitted[4]).intValue(),
//								 Integer.valueOf(splitted[5]).intValue());
//					}else{
//						if (splitted.length == 7){
//							color = new PalleteColor(this,
//									 Long.valueOf(splitted[0]),
//									 splitted[1] + " " + splitted[2] + " " + splitted[3],
//									 Integer.valueOf(splitted[4]).intValue(),
//									 Integer.valueOf(splitted[5]).intValue(),
//									 Integer.valueOf(splitted[6]).intValue());
//						}else{
//							System.out.println("error parsing pallete color!");
//							System.out.println(line);
//						}
//					}
//				}
//				
//				colors.add(color);
//			});
//		}
//		
//		for (int a = 0; a < repetitions; a++){
//			colors.addAll(colors);
//		}
//	}
//	
//	public void randomize(){
//		Collections.shuffle(colors);
//	}
//	
//	public void orderByRED(){
//		Collections.sort(colors, new Comparator<PalleteColor>() {
//	        @Override
//	        public int compare(PalleteColor c1, PalleteColor c2) {
//	            return Float.compare(c1.getColor().getRed(), c2.getColor().getRed());
//	        }
//	    });
//	}
//	
//	public void orderByGREEN(){
//		Collections.sort(colors, new Comparator<PalleteColor>() {
//	        @Override
//	        public int compare(PalleteColor c1, PalleteColor c2) {
//	            return Float.compare(c1.getColor().getGreen(), c2.getColor().getGreen());
//	        }
//	    });
//	}
//	
//	public void orderByBLUE(){
//		Collections.sort(colors, new Comparator<PalleteColor>() {
//	        @Override
//	        public int compare(PalleteColor c1, PalleteColor c2) {
//	            return Float.compare(c1.getColor().getBlue(), c2.getColor().getBlue());
//	        }
//	    });
//	}
//	
//	public void orderByLuminescence(){
//		Collections.sort(colors, new Comparator<PalleteColor>() {
//	        @Override
//	        public int compare(PalleteColor c1, PalleteColor c2) {
//	            return Float.compare(((float) c1.getColor().getRed() * 0.299f + (float) c1.getColor().getGreen() * 0.587f
//	                    + (float) c1.getColor().getBlue() * 0.114f) / 256f, ((float) c2.getColor().getRed() * 0.299f + (float) c2.getColor().getGreen()
//	                    * 0.587f + (float) c2.getColor().getBlue() * 0.114f) / 256f);
//	        }
//	    });
//	}
//	
//	public PalleteColor getColor(int index){
//		if (index >= colors.size()){
//			return null;
//		}
//		
//		return colors.get(index);
//	}
//}