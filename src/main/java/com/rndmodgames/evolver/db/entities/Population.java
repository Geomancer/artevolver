package com.rndmodgames.evolver.db.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "population")
public class Population implements Serializable{

	private static final long serialVersionUID = -629293076417846934L;
	
	private Long id;
	private Experiment experiment;
	private List <Drawing> drawings;
	
	private Double bestScore;
	private Drawing bestDrawing;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "experiment_id", nullable = false)
	public Experiment getExperiment() {
		return experiment;
	}

	public void setExperiment(Experiment experiment) {
		this.experiment = experiment;
	}

	@OneToMany(mappedBy="population", fetch = FetchType.EAGER)
	public List<Drawing> getDrawings() {
		return drawings;
	}
	
	public void setDrawings(List<Drawing> drawings) {
		this.drawings = drawings;
	}

	public Double getBestScore() {
		return bestScore;
	}

	public void setBestScore(Double bestScore) {
		this.bestScore = bestScore;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "best_drawing_id", nullable = true)
	public Drawing getBestDrawing() {
		return bestDrawing;
	}

	public void setBestDrawing(Drawing bestDrawing) {
		this.bestDrawing = bestDrawing;
	}
}