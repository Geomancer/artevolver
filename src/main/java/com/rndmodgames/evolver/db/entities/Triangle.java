package com.rndmodgames.evolver.db.entities;

import java.awt.Color;
import java.awt.Polygon;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "triangle")
public class Triangle extends Polygon{

	private static final long serialVersionUID = -181164485692814464L;
	
	private Long id;
	private Drawing drawing;
	private PaletteColor color;
	private int [] xPoly;
	private int [] yPoly;
	private int lenght;
	
	// default constructor for Hibernate
	public Triangle(){
		
	}
	
	public Triangle(int [] xPoly, int [] yPoly, int lenght, PaletteColor color){
		super(xPoly, yPoly, lenght);
		this.color = color;
		this.xPoly = xPoly;
		this.yPoly = yPoly;
		this.lenght = lenght;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "color_id", nullable = false)
	public PaletteColor getColor() {
		return color;
	}

	public void setColor(PaletteColor color) {
		this.color = color;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "drawing_id", nullable = false)
	public Drawing getDrawing() {
		return drawing;
	}

	public void setDrawing(Drawing drawing) {
		this.drawing = drawing;
	}

	@Column
	public int[] getxPoly() {
		return xPoly;
	}

	public void setxPoly(int[] xPoly) {
		this.xPoly = xPoly;
		super.xpoints = xPoly;
	}

	@Column
	public int[] getyPoly() {
		return yPoly;
	}

	public void setyPoly(int[] yPoly) {
		this.yPoly = yPoly;
		super.ypoints = yPoly;
	}

	@Column
	public int getLenght() {
		return lenght;
	}

	public void setLenght(int lenght) {
		this.lenght = lenght;
		super.npoints = lenght;
	}
}