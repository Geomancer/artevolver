package com.rndmodgames.evolver.viewmodel;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.image.AImage;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;

import com.rndmodgames.evolver.ImageEvolverOLD;
import com.rndmodgames.evolver.db.entities.Palette;
import com.rndmodgames.evolver.db.repositories.DrawingRepo;
import com.rndmodgames.evolver.db.repositories.ExperimentRepo;
import com.rndmodgames.evolver.db.repositories.PaletteColorRepo;
import com.rndmodgames.evolver.db.repositories.PaletteRepo;
import com.rndmodgames.evolver.db.repositories.PopulationRepo;
import com.rndmodgames.evolver.db.repositories.TriangleRepo;

public class EvolverVM {

	/**
	 * 	http://192.155.84.46:28080/ArtEvolver/dashboard.zul
	 *  http://localhost:28080/ArtEvolver/dashboard.zul
	 *  
	 *  Requirements:
	 *  	- Pick image
	 *  	- Set parameters
	 *  	- Process
	 */
	
	// db
	@Autowired private PaletteRepo paletteRepo;
	@Autowired private PaletteColorRepo paletteColorRepo;
	@Autowired private ExperimentRepo experimentRepo;
	@Autowired private DrawingRepo drawingRepo;
	@Autowired private TriangleRepo triangleRepo;
	@Autowired private PopulationRepo populationRepo;
	
	// binded
	private List<Palette> palettes;
	private Palette selectedPalette;
	private ImageEvolverOLD evolver;
	private Long delayMillis = 5000L;
	
	private String [] scalingLevels = {"100%", "50%", "25%"};
	private String selectedScalingLevel = "100%";
	
	// HARDCODED FOR Sherwin-Williams at 100% scale (all the colors)
	int WIDTH_IN_TRIANGLES  = 24; // 48
	int HEIGHT_IN_TRIANGLES = 44; // 44
	
	// PARAMETERS (TODO: move to database for each experiment)
	static final int RANDOM_JUMP_MAX_DISTANCE	= 1;

	public String getSelectedScalingLevel() {
		return selectedScalingLevel;
	}

	public void setSelectedScalingLevel(String selectedScalingLevel) {
		this.selectedScalingLevel = selectedScalingLevel;
	}

	private AImage uploaded;
	
	// stuff
	static Logger log = Logger.getLogger(EvolverVM.class.getName());
	
	@Init
	public void init() throws Exception {
		
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		
		palettes = paletteRepo.getAll();
		
		// if not empty initialize listbox
		if (palettes != null && !palettes.isEmpty()){
			selectedPalette = palettes.get(0);
		}
		
//		evolver = new ImageEvolver();
		
//		
//		Experiment experiment = experimentRepo.get(1L);
//		
//		if (experiment == null){
//			experiment = new Experiment();
//			
//			// TODO: enforce some max values on evolver!
//			experiment.setMaxSize(1L);
//			experiment.setPopulationSize(16L);
//			experiment.setPalette(selectedPalette);
//			experiment.setIterations(0L);
//			experiment.setFinished(false);
//			
//		}else{
//			// set original image
//			InputStream in = new ByteArrayInputStream(experiment.getTargetImage());
//			BufferedImage targetImage = ImageIO.read(in);
//			evolver.setOriginalImage(targetImage);
//			evolver.setFramebuffer(Utils.getFramebuffer("Sherwin-Williams[100%]"));
//		}
//
//		evolver.setExperiment(experiment);
//
//		// if there is no associated colors, we load them from the txt (first run)
//		if (selectedPalette.getColors() == null || selectedPalette.getColors().isEmpty()){
//			evolver.loadPaletteColorsFromTxt("/sherwin.txt");
//			
//			// persist the colors to avoid loading again
//			Palette uploaded = evolver.getExperiment().getPalette();
//			
//			for (PaletteColor color : uploaded.getColors()){
//				paletteColorRepo.save(color);
//			}
//		}else{
//			// initialize the colors for the test palette
//			List<PaletteColor> colors = paletteColorRepo.getAll(selectedPalette.getId());
//			evolver.getExperiment().getPalette().setColors(colors);
//		}
	}
	
	@Command()
	@NotifyChange("*")
	public void uploadImage(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx) {
		
		// only upload if current experiment dont have image
//		if (evolver.getOriginalImage() != null){
//			Clients.showNotification("The current experiment already has an image");
//			return;
//		}
//		
//		UploadEvent upEvent = null;
//		
//		Object objUploadEvent = ctx.getTriggerEvent();
//		
//		if (objUploadEvent != null && (objUploadEvent instanceof UploadEvent)) {
//			upEvent = (UploadEvent) objUploadEvent;
//		}
//		
//		if (upEvent != null) {
//			Media media = upEvent.getMedia();
//			
//			if (media instanceof Image) {
//				
//				// set image as target in image evolver
//				// we do do any processing to the original images
//				BufferedImage bi;
//				
//				try {
//					// generate the buffered image and set it to the evolver
//					bi = ImageIO.read(media.getStreamData());
//					evolver.setOriginalImage(bi);
//					
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					log.severe(e.getMessage());
//				}
//			}
//		}
	}
	
	@Command()
	@NotifyChange("*")
	public void updateScaling() throws Exception{
		
	}
	
	@Command
	@NotifyChange("*")
	public void updateStatus() throws Exception{
		
//		System.out.println("updateStatus()");
		
		// if the experiment isn't set up do nothing
//		if (evolver.getExperiment() == null || evolver.getExperiment().getId() == null){
//			return;
//		}
//		
//		Long experimentID = evolver.getExperiment().getId();
//		Experiment updated = experimentRepo.get(experimentID);
//		
//		if (updated != null){
//			evolver.setExperiment(updated);
//			evolver.update();
//		}
	}
	
	@Command()
	@NotifyChange("*")
	public void start() throws SQLException, IOException {
		
		// validate uploaded image
//		if (evolver.getOriginalImage() == null){
//			Clients.showNotification("Must upload an image first!");
//			return;
//		}
//		
//		// TODO: needs further validations?
//		// set the target image to the experiment, and persist
//		
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
//		ImageIO.write(evolver.getOriginalImage(), "jpg", baos);
//		evolver.getExperiment().setTargetImage(baos.toByteArray());
//		
//		// set as running to initialize safely
//		evolver.getExperiment().setRunning(true);
//		experimentRepo.save(evolver.getExperiment());
//		
//		// TODO: unhardcode values, should be generated acording to target image ratio
//		// TODO: also scaling should afect this value
//		if (evolver.getStatus().equals("Waiting")){
//			evolver.initialize(selectedPalette.getWidth().floatValue(),
//							   selectedPalette.getHeight().floatValue(),
//							   WIDTH_IN_TRIANGLES,
//							   HEIGHT_IN_TRIANGLES);
//		}
//		
//		// try to persist all in the same transaction to speed up processing
//		try {
//			experimentRepo.persist(evolver.getExperiment());
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	@Command()
	@NotifyChange("*")
	public void stop() {
		// validate uploaded image
//		if (evolver.getOriginalImage() == null){
//			Clients.showNotification("Must upload an image first!");
//			return;
//		}
//		
//		// TODO: implement
//		if (evolver.getStatus().equals("Processing")){
//			evolver.setStatus("Stopped");
//		}
	}

	public List<Palette> getPalettes() {
		return palettes;
	}

	public void setPalettes(List<Palette> palettes) {
		this.palettes = palettes;
	}

	public Palette getSelectedPalette() {
		return selectedPalette;
	}

	public void setSelectedPalette(Palette selectedPalette) {
		this.selectedPalette = selectedPalette;
	}

	public ImageEvolverOLD getEvolver() {
		return evolver;
	}

	public void setEvolver(ImageEvolverOLD evolver) {
		this.evolver = evolver;
	}

	public AImage getUploaded() {
		return uploaded;
	}

	public void setUploaded(AImage uploaded) {
		this.uploaded = uploaded;
	}

	public String[] getScalingLevels() {
		return scalingLevels;
	}

	public void setScalingLevels(String[] scalingLevels) {
		this.scalingLevels = scalingLevels;
	}

	public Long getDelayMillis() {
		return delayMillis;
	}

	public void setDelayMillis(Long delayMillis) {
		this.delayMillis = delayMillis;
	}

	@AfterCompose
    public void initSetup(@ContextParam(ContextType.VIEW) Component view) throws Exception {
		Selectors.wireComponents(view, this, false);
	}
}