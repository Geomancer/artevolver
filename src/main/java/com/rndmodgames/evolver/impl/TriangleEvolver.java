package com.rndmodgames.evolver.impl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Stream;

import com.rndmodgames.evolver.Evolver;
import com.rndmodgames.evolver.Utils;
import com.rndmodgames.evolver.db.entities.Drawing;
import com.rndmodgames.evolver.db.entities.PaletteColor;
import com.rndmodgames.evolver.db.entities.Triangle;

import ec.util.MersenneTwisterFast;

public class TriangleEvolver extends Evolver{
	
	static Logger log = Logger.getLogger(TriangleEvolver.class.getName());

	public TriangleEvolver(int size, int maxPopulationSize) throws IOException{

		// initialize evolver
		super(size, maxPopulationSize);
	}
	
	public void init(BufferedImage original, float scale, float width, float height, int widthInTriangles, int heightInTriangles){
		
		// set original image
		this.original = original;
		
		// generate the framebuffer for triangle rendering (same size)
		framebuffer = Utils.getFramebuffer("Sherwin-Williams[100%]", scale);
		
		// initialize and draw the resized original for comparison purposes
		resized = Utils.getScaled(this.original, framebuffer);
		
		// generate
		for (int a = 0; a < populationSize; a++){
			population.add(generateRandomDrawing(scale, width, height, widthInTriangles, heightInTriangles));
			
			// set the drawing ID for duplicate detection
			population.get(a).setId(NEXT_ID);
			
			// increment ID
			NEXT_ID++;
			
			// render initial
			Utils.renderDrawingToFramebuffer(population.get(a), framebuffer);

			// compare rendered child framebuffer with resized original
			double score = Utils.compare(resized, framebuffer);
			population.get(a).setScore(score);
			
			if (score > bestScore){
				bestScore = score;
				bestImage = framebuffer;

				System.out.println("better score found: " + bestScore);
			}
		}

		// the first generated image is the always the best (score already calculated)
		bestImage = framebuffer;
	}
	
	/**
	 * 	Crossover:
	 * 		- picks two random Drawings from the population list
	 * 		- single triangle crossover
	 * 		- score offsprings
	 * 		- keep best scores
	 * 		- prune each n results
	 * 		- max result size
	 */
	public void crossover(int iterations){

		int mutationIterationSuccess = 0;
		Drawing parentA = null;
		Drawing parentB = null;
		
		for (int a = 0; a < iterations; a++) {
			
			// get first parent
			parentA = population.get(roll(population.size()));

			// avoid picking same parent twice
			do{
				
				// pick random parent b
				parentB = population.get(roll(population.size()));

			} while (parentB == null || parentA.getId().equals(parentB.getId()));
			
			Drawing child = new Drawing();
			
			// base parent chance 50/50
			Boolean isParentA = random.nextBoolean();
			
			// create a copy of the triangles to avoid messing with parents
			if (isParentA){
				for (Triangle triangle : parentA.getTriangles()){
					Triangle copy = new Triangle(triangle.getxPoly(), triangle.getyPoly(), triangle.getLenght(), triangle.getColor());
					child.getTriangles().add(copy);
				}
			}else{
				for (Triangle triangle : parentB.getTriangles()){
					Triangle copy = new Triangle(triangle.getxPoly(), triangle.getyPoly(), triangle.getLenght(), triangle.getColor());
					child.getTriangles().add(copy);
				}
			}
			
			// crossover triangles
			// TODO parametrize
			int crossovers = 1;
			
			for (int i = 0; i < crossovers; i++){
				
				// crossover with the other parent
				if (isParentA){
					crossOverRandomColor(child, parentB);
				}else{
					crossOverRandomColor(child, parentA);
				}
			}
			
			// evaluate child and insert if better
			Utils.renderDrawingToFramebuffer(child, framebuffer);
			
			// compare rendered child framebuffer with resized original
			double score = Utils.compare(resized, framebuffer);
			child.setScore(score);
			
			// if best score update stats
			if (score > bestScore){
				bestScore = score;
				bestImage = framebuffer;
				mutationIterationSuccess++;
				
				// add child with generated ID to population
				child.setId(NEXT_ID);
				population.add(child);
				
				// increment ID
				NEXT_ID++;
				
				// TODO: validate max population size / pruning
//				System.out.println(mutationIterationSuccess + ": better score found: " + bestScore + " in iteration: " + a);
			}
			
			// TODO parametrize prune size
			float PRUNE_PERCENT = 1f;
			
			if ((float) population.size() > (float) maxPopulationSize * PRUNE_PERCENT){
				
				Collections.sort(population, new DrawingComparator());
				
				// remove worst
				population.remove(0);
			}
			
			// print stats for excel import
			if (a % 1000 == 0 && a != 0){
				System.out.println(a + ", " + population.size() + ", " + bestScore + ", " + mutationIterationSuccess + ", " + ((float)mutationIterationSuccess/(float)a));
			}
		}
	}
	
	public static void crossOverRandomColor(Drawing child, Drawing parent){
		Triangle origin = parent.getTriangles().get(roll(parent.getTriangles().size()));
		Triangle dest = child.getTriangles().get(roll(child.getTriangles().size()));
		
		PaletteColor aux = origin.getColor();
		origin.setColor(dest.getColor());
		dest.setColor(aux);
	}
	
	public class DrawingComparator implements Comparator<Drawing> {
	    @Override
	    public int compare(Drawing o1, Drawing o2) {
	    	return o1.getScore().compareTo(o2.getScore());
	    }
	}
	
	public void closeMutation(int iterations){
//		
//		long then = System.currentTimeMillis();		
//		int mutationCount = 0;
//		int mutationIterationSuccess = 0;
		
		// TODO: parametrize
		int closeJumpMaxDistance = 4;
		
		for (int a = 0; a < iterations; a++){
			
			// get random drawing
			Drawing drawing = population.get(roll(population.size()));

			switchCloseColor(drawing, closeJumpMaxDistance);

			// render
			Utils.renderDrawingToFramebuffer(drawing, framebuffer);

			// set base score
			double score = Utils.compare(resized, framebuffer);
			
			if (score > bestScore){
				bestScore = score;
				bestImage = framebuffer;
//				mutationIterationSuccess++;
				
//				System.out.println(mutationIterationSuccess + ": better score found: " + bestScore);
			}
		}

//		long now = System.currentTimeMillis();
//		System.out.println("TriangleEvolver.closeMutation() Finished. " + iterations + " mutations with " + mutationIterationSuccess + " successes. Took " + (now-then) + " ms");
	}
	
	/**
	 * 
	 * @param iterations
	 */
	public void randomMutation(int iterations){
		
//		long then = System.currentTimeMillis();		
//		int mutationCount = 0;
//		int mutationIterationSuccess = 0;
		
		for (int a = 0; a < iterations; a++){
			
			// get random drawing
			Drawing drawing = population.get(roll(population.size()));

			switchRandomColor(drawing);

			// render
			Utils.renderDrawingToFramebuffer(drawing, framebuffer);

			// set base score
			double score = Utils.compare(resized, framebuffer);
			
			if (score > bestScore){
				bestScore = score;
				bestImage = framebuffer;
//				mutationIterationSuccess++;
				
//				System.out.println(mutationIterationSuccess + ": better score found: " + bestScore);
			}
		}

//		long now = System.currentTimeMillis();
//		System.out.println("TriangleEvolver.randomMutation() Finished. " + iterations + " mutations with " + mutationIterationSuccess + " successes. Took " + (now-then) + " ms");
	}
	
	public static void switchRandomColor(Drawing drawing){
		Triangle origin = drawing.getTriangles().get(roll(drawing.getTriangles().size()));
		Triangle dest = drawing.getTriangles().get(roll(drawing.getTriangles().size()));
		
		PaletteColor aux = origin.getColor();
		origin.setColor(dest.getColor());
		dest.setColor(aux);
	}
	
	public static void switchCloseColor(Drawing drawing, int maxDistance){
	
		int pos = roll(drawing.getTriangles().size());
		int des = 0;
		int jump = roll(maxDistance) + 1;
		
		if(random.nextBoolean()){
			des = pos + jump;
		}else{
			des = pos - jump;
		}
	
		if (des < 0){
			des = (drawing.getTriangles().size() - 1) - pos - jump;
		}
	
		if (des >= drawing.getTriangles().size()){
			des = 0 + (drawing.getTriangles().size() - pos + jump);
		}
		
		Triangle origin = drawing.getTriangles().get(pos);
		Triangle dest = drawing.getTriangles().get(des);
		
		PaletteColor aux = origin.getColor();
		origin.setColor(dest.getColor());
		dest.setColor(aux);
	}
	
	// TODO: move image comparison outside evolver 
	public double compare(BufferedImage img1, BufferedImage img2) {
		
		int width1 = img1.getWidth(null);
		int width2 = img2.getWidth(null);
		int height1 = img1.getHeight(null);
		int height2 = img2.getHeight(null);
		
		if ((width1 != width2) || (height1 != height2)) {
			System.err.println("Error: Images dimensions mismatch");
			return 0;
		}
		
		boolean fitnessByColor = true;
		long diff = 0;
		
		if (fitnessByColor){
			for (int y = 0; y < height1; y++) {
				for (int x = 0; x < width1; x++) {
					int rgb1 = img1.getRGB(x, y);
					int rgb2 = img2.getRGB(x, y);
					int r1 = (rgb1 >> 16) & 0xff;
					int g1 = (rgb1 >> 8) & 0xff;
					int b1 = (rgb1) & 0xff;
					int r2 = (rgb2 >> 16) & 0xff;
					int g2 = (rgb2 >> 8) & 0xff;
					int b2 = (rgb2) & 0xff;
					diff += Math.abs(r1 - r2);
					diff += Math.abs(g1 - g2);
					diff += Math.abs(b1 - b2);
				}
			}
		}else{
			for (int y = 0; y < height1; y++) {
				for (int x = 0; x < width1; x++) {
					int rgb1 = img1.getRGB(x, y);
					int rgb2 = img2.getRGB(x, y);
					
					float r1 = ((rgb1 >> 16) & 0xff) * 1f; 	// 0.299f
					float g1 = ((rgb1 >> 8) & 0xff) * 0f;	// 0.587f
					float b1 = ((rgb1) & 0xff) * 0f; 		// 0.114f
					
					float r2 = ((rgb2 >> 16) & 0xff) * 1f;
					float g2 = ((rgb2 >> 8) & 0xff) * 0f;
					float b2 = ((rgb2) & 0xff) * 0f;
					
					diff += Math.abs(r1 - r2);
					diff += Math.abs(g1 - g2);
					diff += Math.abs(b1 - b2);
				}
			}
		}
	
		double n = width1 * height1 * 3;
		double p = diff / n / 255.0;
		return 1 - p;
	}
	

	
	// only works for triangles so better name or @override
	private Drawing generateRandomDrawing(float scale, float width, float height, int widthInTriangles, int heightInTriangles){
		
		Drawing drawing = new Drawing();
		
		// 
		ArrayList<Triangle> triangles = new ArrayList<Triangle>();
		
		// TODO: randomize palette
		if (true){
			Collections.shuffle(colors);
		}
		
		// keep count of current color to avoid repeating
		int count = 0;
		
		for (int a = 0; a < widthInTriangles; a++){
			for (int b = 0; b < heightInTriangles; b++){
				int xPoly [] = new int [3];
    			int yPoly [] = new int [3];
    			
    			if (b % 2 == 0){
					xPoly[0] = (int) (width * scale * a);
					xPoly[1] = (int) ((width * scale * a) + (width * scale));
					xPoly[2] = (int) (width * scale * a);
					
					yPoly[0] = (int) ((height * scale * b));
					yPoly[1] = (int) ((height * scale * b));
					yPoly[2] = (int) ((height * scale * b) + (height * scale));
    			}else{
    				xPoly[0] = (int) ((width * scale * a));
					xPoly[1] = (int) ((width * scale * a) + (width * scale));
					xPoly[2] = (int) ((width * scale * a) + (width * scale));
					
					yPoly[0] = (int) ((height * scale * b));
					yPoly[1] = (int) ((height * scale * b));
					yPoly[2] = (int) ((height * scale * b) - (height * scale));
    			}
    			
    			// dynamic row shifting
				yPoly[0] -= (height * scale) * (b / 2);
				yPoly[1] -= (height * scale) * (b / 2);
				yPoly[2] -= (height * scale) * (b / 2);

				PaletteColor color = null;
				
				if (colors.get(count) != null){
					color = colors.get(count);
					count++;
				}
				
				Triangle triangle = new Triangle(xPoly, yPoly, 3, color);
        		triangles.add(triangle);
			}
		}

		// set generated triangles
		drawing.setTriangles(triangles);

		Utils.renderDrawingToFramebuffer(drawing, framebuffer);
		
		// set base score
		bestScore = Utils.compare(resized, framebuffer);
		
		return drawing;
	}
}