package com.rndmodgames.evolver.impl;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

import com.rndmodgames.evolver.Evolver;
import com.rndmodgames.evolver.Utils;
import com.rndmodgames.evolver.db.entities.Drawing;
import com.rndmodgames.evolver.db.entities.PaletteColor;
import com.rndmodgames.evolver.db.entities.Triangle;

public class GreedyEvolver extends Evolver{

	public GreedyEvolver(int size, int maxPopulationSize) throws IOException {
		
		// initialize
		super(size, maxPopulationSize);
	}

	/**
	 * Strategies:
	 * 	- picking color
	 * 	- picking position
	 * 	
	 * 	- random
	 * 	- one by one
	 */
	public void generate(BufferedImage original, float scale, float width, float height, int widthInTriangles, int heightInTriangles, boolean randomizePick, boolean randomizePosition){
		
		// set original image
		this.original = original;
		
		// generate the framebuffer for triangle rendering (same size)
		this.framebuffer = Utils.getFramebuffer("Sherwin-Williams[100%]", scale);
		this.bestImage = Utils.getFramebuffer("Sherwin-Williams[100%]", scale);
		
		// initialize and draw the resized original for comparison purposes
		this.resized = Utils.getScaled(this.original, framebuffer);
		
		//
		Drawing drawing = new Drawing();
		
		// initialize triangle grid 
		ArrayList<Triangle> triangles = new ArrayList<Triangle>();
		
		for (int a = 0; a < widthInTriangles; a++){
			for (int b = 0; b < heightInTriangles; b++){
				int xPoly [] = new int [3];
    			int yPoly [] = new int [3];
    			
    			if (b % 2 == 0){
					xPoly[0] = (int) (width * scale * a);
					xPoly[1] = (int) ((width * scale * a) + (width * scale));
					xPoly[2] = (int) (width * scale * a);
					
					yPoly[0] = (int) ((height * scale * b));
					yPoly[1] = (int) ((height * scale * b));
					yPoly[2] = (int) ((height * scale * b) + (height * scale));
    			}else{
    				xPoly[0] = (int) ((width * scale * a));
					xPoly[1] = (int) ((width * scale * a) + (width * scale));
					xPoly[2] = (int) ((width * scale * a) + (width * scale));
					
					yPoly[0] = (int) ((height * scale * b));
					yPoly[1] = (int) ((height * scale * b));
					yPoly[2] = (int) ((height * scale * b) - (height * scale));
    			}
    			
    			// dynamic row shifting
				yPoly[0] -= (height * scale) * (b / 2);
				yPoly[1] -= (height * scale) * (b / 2);
				yPoly[2] -= (height * scale) * (b / 2);

				// triangle with empty color
				Triangle triangle = new Triangle(xPoly, yPoly, 3, null);
        		triangles.add(triangle);
			}
		}

		// set generated triangles
		drawing.setTriangles(triangles);
		
		// add this grid to population
		population.add(drawing);
	}
	
	public void iterate(){

		Drawing drawing = population.get(0);
		
		int bestTriangle = -1;
		int internalBestTriangle = -1;
		
		double internalBestScore = 0d;
		double score = 0d;
	
		// pick one color
		PaletteColor picked = colors.get(roll(colors.size()));
			
		// iterate all possible positions and check score
		for (int a = 0; a < drawing.getTriangles().size(); a++){
				
			if (drawing.getTriangles().get(a).getColor() != null){
				continue;
			}
			
			// set color
			drawing.getTriangles().get(a).setColor(picked);
			
			// render
			Utils.renderDrawingToFramebuffer(drawing, framebuffer);
			
			// calculate score
			score = Utils.compare(resized, framebuffer);
			
			if (score > internalBestScore){
				internalBestScore = score;
				internalBestTriangle = a;
			}
			
			if (score > bestScore){
				bestScore = score;
				bestTriangle = a;
			}
			
			// clear color
			drawing.getTriangles().get(a).setColor(null);
		}
		
		// set a triangle to avoid setting again in 0
		if (bestTriangle == -1){
			bestTriangle = internalBestTriangle;
		}
		
		// set the best selected color
		drawing.getTriangles().get(bestTriangle).setColor(picked);
		
		// remove picked
		colors.remove(picked);
		
		// print stats
//		System.out.println(colors.size() + ", " + bestScore);
		
		Utils.renderDrawingToFramebuffer(drawing, framebuffer);
		
		// set base score
		bestScore = Utils.compare(resized, framebuffer);
		
		// set best drawing
		Utils.renderDrawingToFramebuffer(drawing, bestImage);
	}
}