package com.rndmodgames.evolver;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.util.HashMap;
import java.util.Map;

import com.rndmodgames.evolver.db.entities.Drawing;
import com.rndmodgames.evolver.db.entities.Triangle;

import ec.util.MersenneTwisterFast;

public class Utils {

	public static MersenneTwisterFast random = new MersenneTwisterFast();
	public static Color background;
	public static BufferedImage backgroundImage;
	public static boolean mirror = true;
	
	/**
	 * Converts a given Image into a BufferedImage
	 *
	 * @param img The Image to be converted
	 * @return The converted BufferedImage
	 */
	public static BufferedImage toBufferedImage(Image img){
	    if (img instanceof BufferedImage)
	    {
	        return (BufferedImage) img;
	    }

	    // Create a buffered image with transparency
	    BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

	    // Draw the image on to the buffered image
	    Graphics2D bGr = bimage.createGraphics();
	    bGr.drawImage(img, 0, 0, null);
	    bGr.dispose();

	    // Return the buffered image
	    return bimage;
	}
	
	public static BufferedImage getFramebuffer(String size, float scale) {
		switch (size){
		case "240p":
			return new BufferedImage(320, 240, BufferedImage.TYPE_INT_ARGB);
			
		case "Sherwin-Williams[100%]":
			
//			return new BufferedImage((int)(38.0f * 2.0f * 5.1f * scale / 2f),
//									 (int)(56.0f * 3.8f * scale / 2f),
//									 BufferedImage.TYPE_INT_ARGB);
			
			return new BufferedImage((int)(24.0f * 2.0f * 5.1f * scale / 2f),
									 (int)(44.0f * 3.8f * scale / 2f),
									 BufferedImage.TYPE_INT_ARGB);
			
		default:
			return new BufferedImage(320, 240, BufferedImage.TYPE_INT_ARGB);
		}
	}
	
	/*
	 * Where bi is your image, (x0,y0) is your upper left coordinate, and (w,h)
	 * are your width and height respectively
	 */
	public static Color averageColor(BufferedImage bi, int x0, int y0, int w, int h) {
	    int x1 = x0 + w;
	    int y1 = y0 + h;
	    long sumr = 0, sumg = 0, sumb = 0;
	    for (int x = x0; x < x1; x++) {
	        for (int y = y0; y < y1; y++) {
	            Color pixel = new Color(bi.getRGB(x, y));
	            sumr += pixel.getRed();
	            sumg += pixel.getGreen();
	            sumb += pixel.getBlue();
	        }
	    }
	    int num = w * h;
	    return new Color((int)sumr / num, (int)sumg / num, (int)sumb / num);
	}
	
	private static Map<String, Color> cacheColor = new HashMap<>();
	
	public static void renderDrawingToFramebuffer(Drawing drawing, BufferedImage framebuffer){
		Graphics2D g = (Graphics2D)framebuffer.getGraphics();
		
		// parametrize
		g.setRenderingHint(RenderingHints.KEY_RENDERING,
		   		   		   RenderingHints.VALUE_RENDER_QUALITY);
		
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
						   RenderingHints.VALUE_ANTIALIAS_ON);
		
		// fill white for color comparison
		if (background == null){
			
			// if image set, mirror, else default is black
			if (backgroundImage != null){

				Polygon polygon = new Polygon();
				
				polygon.addPoint(0, 0);
				polygon.addPoint(0, backgroundImage.getWidth());
				polygon.addPoint(backgroundImage.getWidth(), backgroundImage.getWidth());
				polygon.addPoint(backgroundImage.getWidth(), 0);
				
				g.setClip(polygon);

				// draw
				g.drawImage(backgroundImage, 0, 0, null);

			} else{
				
				// default canvas is black
				g.setColor(Color.BLACK);
			}
		}else{
			
			// set the color and render a full rectangle
			g.setColor(background);
			g.fillRect(0, 0, framebuffer.getWidth(), framebuffer.getHeight());
		}

		// set the first image as best image
		for (Triangle triangle : drawing.getTriangles()){
			if (triangle.getColor() != null) {

				Color color = new Color(triangle.getColor().getR(), triangle.getColor().getG(), triangle.getColor().getB());
	
				g.setColor(color);
				g.drawPolygon(triangle);
				g.fillPolygon(triangle);
				
			} else {
				// only render blue triangles for debug
//				g.setColor(Color.BLUE);
//				g.drawPolygon(triangle);
			}
		}
	}
	
	/**
	 * @param image
	 * @param scale (string representing percents)
	 * @return A scaled clone of the original image
	 */
	public static BufferedImage getScaled(BufferedImage image, String scale) {
		
		float targetScale = 1.0f;
		
		if (scale != null){
			switch (scale){
			case "50%":
				targetScale = 0.5f;
				break;
			case "25%":
				targetScale = 0.25f;
				break;
			default:
				targetScale = 1.0f;
			}
		}
		
		int w = image.getWidth();
		int h = image.getHeight();
		
		// creates an empty image with the original image size
		BufferedImage after = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		AffineTransform at = new AffineTransform();
		at.scale(targetScale, targetScale);
		
		// TODO: parametrized transform settings (TYPE, ETC)
		AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
		
		// scale to the desired settings
		after = scaleOp.filter(image, after);

		return after;
	}
	
	public static BufferedImage getScaled(BufferedImage image, BufferedImage target) {
		
		double xScale = (double)((double)target.getWidth() / (double)image.getWidth());
		double yScale = (double)((double)target.getHeight() / (double)image.getHeight());

		// creates an empty image with the original image size
		BufferedImage after = new BufferedImage(target.getWidth(), target.getHeight(), BufferedImage.TYPE_INT_ARGB);
		AffineTransform at = new AffineTransform();
		at.scale(xScale, yScale);
		
		// TODO: parametrized transform settings (TYPE, ETC)
		AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
		
		// scale to the desired settings
		after = scaleOp.filter(image, after);
		return after;
	}
	
	public static double compare(BufferedImage img1, BufferedImage img2){
		int width1 = img1.getWidth(null);
		int width2 = img2.getWidth(null);
		int height1 = img1.getHeight(null);
		int height2 = img2.getHeight(null);
		
		if ((width1 != width2) || (height1 != height2)) {
			System.err.println("Error: Images dimensions mismatch");
			return 0;
		}
		
		boolean fitnessByColor = true;
		long diff = 0;
		
		if (fitnessByColor){
			for (int y = 0; y < height1; y++) {
				for (int x = 0; x < width1; x++) {
					int rgb1 = img1.getRGB(x, y);
					int rgb2 = img2.getRGB(x, y);
					int r1 = (rgb1 >> 16) & 0xff;
					int g1 = (rgb1 >> 8) & 0xff;
					int b1 = (rgb1) & 0xff;
					int r2 = (rgb2 >> 16) & 0xff;
					int g2 = (rgb2 >> 8) & 0xff;
					int b2 = (rgb2) & 0xff;
					diff += Math.abs(r1 - r2);
					diff += Math.abs(g1 - g2);
					diff += Math.abs(b1 - b2);
				}
			}
		}else{
			for (int y = 0; y < height1; y++) {
				for (int x = 0; x < width1; x++) {
					int rgb1 = img1.getRGB(x, y);
					int rgb2 = img2.getRGB(x, y);
					
					float r1 = ((rgb1 >> 16) & 0xff) * 1f; 	// 0.299f
					float g1 = ((rgb1 >> 8) & 0xff) * 0f;	// 0.587f
					float b1 = ((rgb1) & 0xff) * 0f; 		// 0.114f
					
					float r2 = ((rgb2 >> 16) & 0xff) * 1f;
					float g2 = ((rgb2 >> 8) & 0xff) * 0f;
					float b2 = ((rgb2) & 0xff) * 0f;
					
					diff += Math.abs(r1 - r2);
					diff += Math.abs(g1 - g2);
					diff += Math.abs(b1 - b2);
				}
			}
		}
	
		double n = width1 * height1 * 3;
		double p = diff / n / 255.0;
		return 1 - p;
	}
	
	public static int random(int n){
		return random.nextInt(n) + 1;
	}
	
//	@Override
//	public void run() {
//		
//		// if status == Processing
//		
//		// else wait
//		System.out.println("Running");
//		
//		// 
//		while (status.equals("Processing")){
//			
//			experiment.setIterations(experiment.getIterations() + 1L);
//			System.out.println("iteration: " + experiment.getIterations());
//			
//			try {
//				// benchmark for performance, this is probably needed to stop processing from outside
//				TimeUnit.MILLISECONDS.sleep(10L);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		}
//	}
}