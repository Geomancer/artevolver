package com.rndmodgames.evolver;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import com.rndmodgames.evolver.db.entities.Drawing;
import com.rndmodgames.evolver.db.entities.PaletteColor;

import ec.util.MersenneTwisterFast;

public abstract class Evolver {

	/**
	 * original   : the image privided by the user
	 * resized    : the image privided by the user resized to a small size for performance purposes
	 * framebuffer: the image where all drawing is performed (same size as resized)
	 * bestImage  : the current best image
	 */
	protected BufferedImage original;
	protected BufferedImage resized;
	protected BufferedImage framebuffer;
	protected BufferedImage bestImage;
	
	// TODO: keep ordered by score for faster comparison
	protected int populationSize;
	protected int maxPopulationSize;
	
	protected List <PaletteColor> colors;
	protected List<Drawing> population;
	protected double bestScore;
	
	// TODO: change to getNextID() and do the ++ there
	protected static Long NEXT_ID = 1L;
	public static final MersenneTwisterFast random = new MersenneTwisterFast();
	
	public Evolver (int size, int maxPopulationSize) throws IOException{
		// initialize palette
		if (colors == null){
			loadDefaultPalette();
		}

		// initialize population
		if (population == null){
			population = new ArrayList<>();
		}
		
		// store population size
		this.populationSize = size;
		this.maxPopulationSize = maxPopulationSize;
	}
	
	// TODO: move outside
	public void loadDefaultPalette() throws IOException {
		
		colors = new ArrayList<PaletteColor>();
		
		URL url = getClass().getResource("../../../../sherwin.txt");
		File file = new File(url.getPath());
		
		try (Stream<String>stream = Files.lines(file.toPath(), StandardCharsets.UTF_8)) {
			stream.forEach((line)->{
				PaletteColor color = null;
				String [] splitted = line.split(" ");

				if (splitted.length == 5){
					color = new PaletteColor(splitted[0],
											 splitted[1],
											 Integer.valueOf(splitted[2]).intValue(),
											 Integer.valueOf(splitted[3]).intValue(),
											 Integer.valueOf(splitted[4]).intValue());
				}else{
					if (splitted.length == 6){
						color = new PaletteColor(splitted[0],
									splitted[1] + " " + splitted[2],
									Integer.valueOf(splitted[3]).intValue(),
									Integer.valueOf(splitted[4]).intValue(),
									Integer.valueOf(splitted[5]).intValue());
					}else{
						if (splitted.length == 7){
							color = new PaletteColor(splitted[0],
													 splitted[1] + " " + splitted[2] + " " + splitted[3],
													 Integer.valueOf(splitted[4]).intValue(),
													 Integer.valueOf(splitted[5]).intValue(),
													 Integer.valueOf(splitted[6]).intValue());
						}else{
							System.out.println("error parsing pallete color!");
							System.out.println(line);
						}
					}
				}

				colors.add(color);
			});
		}
		
		// duplicate for palette repeats
//		colors.addAll(colors);
	}

	public static int roll(int n) {
		return random.nextInt(n);
	}
	
	public BufferedImage getBestImage() {
		return bestImage;
	}

	public double getBestScore() {
		return bestScore;
	}

	public void setBestScore(double bestScore) {
		this.bestScore = bestScore;
	}

	public List<PaletteColor> getColors() {
		return colors;
	}

	public void setColors(List<PaletteColor> colors) {
		this.colors = colors;
	}

	public BufferedImage getResized() {
		return resized;
	}

	public void setResized(BufferedImage resized) {
		this.resized = resized;
	}
}